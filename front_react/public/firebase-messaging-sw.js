// eslint-disable-next-line no-undef
importScripts("https://www.gstatic.com/firebasejs/8.6.1/firebase-app.js");
// eslint-disable-next-line no-undef
importScripts("https://www.gstatic.com/firebasejs/8.6.1/firebase-messaging.js");

// eslint-disable-next-line no-undef
firebase.initializeApp({
  apiKey: "AIzaSyAKa2bLPmMdjd_PEZGrHu2BmTH7Oxx3BvQ",
  authDomain: "ecg-telnet.firebaseapp.com",
  databaseURL: "https://ecg-telnet.firebaseio.com",
  projectId: "ecg-telnet",
  storageBucket: "ecg-telnet.appspot.com",
  messagingSenderId: "860540330866",
  appId: "1:860540330866:web:f3def4ce2cd2f6aee633eb",
  measurementId: "G-47E7HRG82Q",
});
// eslint-disable-next-line no-undef
const messaging = firebase.messaging();

messaging.onBackgroundMessage((payload) => {
  console.log(
    "🚀 ~ file: firebase-messaging-sw.js ~ line 21 ~ messaging.onBackgroundMessage ~ payload",
    payload
  );

  // // Customize notification here
  // const notificationTitle = "Background Message Title";
  // const notificationOptions = {
  //   body: "Background Message body.",
  //   icon: "/firebase-logo.png",
  // };

  // // eslint-disable-next-line no-restricted-globals
  // self.registration.showNotification(notificationTitle, notificationOptions);
});
