import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import axios from "axios";
import { getToken, getSession } from "../../Utils/SessionUtils";

const api_url = process.env.REACT_APP_API_BASE_URL;
const config = {
  headers: { Authorization: `Bearer ${getToken()}` },
};

/**
 * get document by code
 */
export async function getDocumentByCode() {
  try {
    const response = await axios.get(api_url + "document/code/" + getSession("code_generated"), config);
    return response;
  } catch (error) {
    console.error(error);
  }
}
/**
 * get annonces by code
 */
export async function getDocumentById(id) {
  try {
    const response = await axios.get(api_url + "document/id/" + id, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * get annonces by code
 */
export async function getDocumentByUserId(userid) {
  try {
    const response = await axios.get(api_url + "annonce/userid/" + userid, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function addDocument(file, type, description) {
  const frmData = new FormData();

  frmData.append("user_id", getSession("id"));
  frmData.append("code_generated", getSession("code_generated"));
  frmData.append("file", file);
  frmData.append("type", type);
  frmData.append("description", description);

  const httpHeaders = {
    "Content-Type": "multipart/form-data",
    Authorization: "Bearer " + getToken(),
  };

  const options = {
    headers: httpHeaders,
  };

  try {
    const response = await axios.post(api_url + "document", frmData, options);
    toast.success("Fichier ajoutée");
    return response;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      toast.error("error server");
      console.log(error.response.data);
      console.log(error.response.message);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the
      // browser and an instance of
      // http.ClientRequest in node.js
      toast.error("error request");
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return error;
  }
}

/**
 * get annonces by code
 */
export async function deleteDocumentById(id) {
  try {
    const response = await axios.delete(api_url + "document/" + id, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}
