import axios from "axios";
import { getToken, getSession } from "./../../Utils/SessionUtils";

const api_url = process.env.REACT_APP_API_BASE_URL;
const config = {
  headers: { Authorization: `Bearer ${getToken()}` },
};

/**
 * get all settings
 */
export async function getAllSettings() {
  try {
    const response = await axios.get(api_url + "settings/" + getSession("code_generated"), config);

    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function postSettings(
  code,
  workHoure,
  startHoure,
  endHoure,
  dayPerWeeks,
  breakTime,
  totalConge,
  monthRetard,
  dayRetard
) {
  const credentiel = {
    code_generated: code,
    workHoure: workHoure,
    startHoure: startHoure,
    endHoure: endHoure,
    dayPerWeeks: dayPerWeeks,
    breakTime: breakTime,
    totalConge: totalConge,
    monthRetard: monthRetard,
    dayRetard: dayRetard,
  };
  try {
    const response = await axios.post(api_url + "settings", credentiel, config);
    //toast.success("Utilisateur ajouté");
    return response;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      // toast.error("error server");
      console.log(error.response.data);
      console.log(error.response.message);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the
      // browser and an instance of
      // http.ClientRequest in node.js
      //toast.error("error request");
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return error;
  }
}
