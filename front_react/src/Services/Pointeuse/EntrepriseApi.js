import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import axios from "axios";
import { getToken, getSession } from "./../../Utils/SessionUtils";

const api_url = process.env.REACT_APP_API_BASE_URL;
const config = {
  headers: { Authorization: `Bearer ${getToken()}` },
};

/**
 * get Entreprise by code
 */
export async function getEntrepriseByCode() {
  try {
    const response = await axios.get(api_url + "societe/" + getSession("code_generated"), config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function updateLogo(logo, code_generated) {
  const frmData = new FormData();
  frmData.append("code_generated", code_generated);
  frmData.append("logo", logo);

  const httpHeaders = {
    "Content-Type": "multipart/form-data",
    Authorization: "Bearer " + getToken(),
  };

  const options = {
    headers: httpHeaders,
  };

  try {
    const response = await axios.put(api_url + "societe/img", frmData, options);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * edit entreprise
 * @param {*} nom
 * @param {*} adresse
 * @param {*} matricule_fiscal
 * @param {*} numero_cnss
 * @param {*} rib
 * @param {*} banque
 * @param {*} mobile
 * @param {*} fax
 * @param {*} website_url
 * @param {*} secteur_activite
 * @param {*} email
 * @param {*} nb_employe
 * @param {*} start_year
 * @param {*} code_generated
 */
export async function editEntreprise(
  nom,
  adresse,
  matricule_fiscal,
  numero_cnss,
  rib,
  banque,
  mobile,
  fax,
  website_url,
  secteur_activite,
  email,
  nb_employe,
  start_year,
  code_generated
) {
  const credentiel = {
    code_generated: code_generated,
    nom: nom,
    adresse: adresse,
    matricule_fiscal: matricule_fiscal,
    numero_cnss: numero_cnss,
    rib: rib,
    banque: banque,
    mobile: mobile,
    fax: fax,
    website_url: website_url,
    email: email,
    secteur_activite: secteur_activite,
    nb_employe: nb_employe,
    start_year: start_year,
  };

  try {
    const response = await axios.put(api_url + "societe", credentiel, config);
    console.log(response);
    toast.success("mise à jour");
    return response;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      toast.error("error server");
      console.log(error.response.data);
      console.log(error.response.message);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the
      // browser and an instance of
      // http.ClientRequest in node.js
      toast.error("error request");
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return error;
  }
}

export async function editLigneFacture(ligne_facture_1, ligne_facture_2, ligne_facture_3, ligne_facture_4) {
  const credentiel = {
    ligne_facture_1: ligne_facture_1,
    ligne_facture_2: ligne_facture_2,
    ligne_facture_3: ligne_facture_3,
    ligne_facture_4: ligne_facture_4,
    code_generated: getSession("code_generated"),
  };

  try {
    const response = await axios.put(api_url + "societe/lignefacture", credentiel, config);
    console.log(response);
    toast.success("mise à jour");
    return response;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      toast.error("error server");
      console.log(error.response.data);
      console.log(error.response.message);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the
      // browser and an instance of
      // http.ClientRequest in node.js
      toast.error("error request");
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return error;
  }
}
