import axios from "axios";
import { getSession, getToken } from "../../Utils/SessionUtils";

const api_url = process.env.REACT_APP_API_BASE_URL;
//header config
const config = {
  headers: { Authorization: `Bearer ${getToken()}` },
};

/**
 * get user by id
 * @param {*} id
 */
export async function getUserById(id) {
  try {
    const response = await axios.get(api_url + "users/" + id, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * get all users
 */
export async function getUsersByCode() {
  try {
    const response = await axios.get(
      api_url + "users/codealluseractif/" + localStorage.getItem("code_generated"),
      config
    );
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * get all actif user
 * @returns array
 */
export async function getUsersActifByCode() {
  try {
    const response = await axios.get(api_url + "users/codealluser/" + localStorage.getItem("code_generated"), config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * get Users with personnel details
 * @returns array
 */
export async function getUsersPersonnelByCode() {
  try {
    const response = await axios.get(
      api_url + "users/codeuserpersonnel/" + localStorage.getItem("code_generated"),
      config
    );
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * login users
 * @param {string} email
 * @param {string} password
 */
export async function userLogin(email, password) {
  const credentiel = { email: email, password: password };
  try {
    const response = await axios.post(api_url + "users/login", credentiel);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * edit user Profil
 * @param {string} email
 * @param {string} password
 */
export async function editUserProfile(id, nom, prenom, fonction, mobile) {
  const credentiel = {
    id: id,
    nom: nom,
    prenom: prenom,
    fonction: fonction,
    mobile: mobile,
  };
  try {
    const response = await axios.patch(api_url + "users/updateprofile", credentiel, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * activate/desactivate user
 * @param {string} id
 * @param {string} isactif
 * @returns
 */
export async function editUserStatus(id, isactif) {
  const credentiel = {
    id: id,
    isactif: isactif,
  };
  try {
    const response = await axios.patch(api_url + "users/updatestatus", credentiel, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * get documments by users id
 */
export async function getDocumentByUserId(userid) {
  try {
    const response = await axios.get(api_url + "annonce/userid/" + userid, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * register new user
 * @param {string} nom
 * @param {string} prenom
 * @param {string} mobile
 * @param {string} date
 * @param {string} email
 * @param {string} password
 * @param {string} code
 * @returns
 */
export async function registerUser(nom, prenom, mobile, date, email, password, code) {
  const credentiel = {
    email: email,
    nom: nom,
    prenom: prenom,
    code_generated: code,
    password: password,
    birth_date: date,
    role_code: "user",
    isactif: "1",
    mobile: mobile,
    fonction: "dev",
    solde_conge: "21",
  };
  try {
    const response = await axios.post(api_url + "users/register", credentiel);
    //toast.success("Utilisateur ajouté");
    return response;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      // toast.error("error server");
      console.log(error.response.data);
      console.log(error.response.message);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the
      // browser and an instance of
      // http.ClientRequest in node.js
      //toast.error("error request");
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return error;
  }
}

/**
 * get all users
 */
export async function getAllUsers() {
  try {
    const response = await axios.get(api_url + "users/getAllUsers/", config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * delete user by id
 * @param {string} id
 * @returns
 */
export async function deleteUserById(id) {
  try {
    const response = await axios.delete(api_url + "users/" + id, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * add code generated
 * @param {string} code
 * @returns
 */
export async function addCode(code) {
  const credentiel = {
    id: getSession("id"),
    code_generated: code,
  };
  try {
    const response = await axios.patch(api_url + "users/", credentiel, config);
    console.log(response);
    return response;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data);
      console.log(error.response.message);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the
      // browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return error;
  }
}
