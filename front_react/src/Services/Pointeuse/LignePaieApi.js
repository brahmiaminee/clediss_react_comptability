import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import axios from "axios";
import { getToken, getSession } from "./../../Utils/SessionUtils";

const api_url = process.env.REACT_APP_API_BASE_URL;
const config = {
  headers: { Authorization: `Bearer ${getToken()}` },
};

/**
 * get ligne paie by code
 */
export async function getLignePaieByCode() {
  try {
    const response = await axios.get(api_url + "lignepaie/" + getSession("code_generated"), config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function addLignePaie(code, code_paie, base, gain, retenue, compte_comptable) {
  const credentiel = {
    code: code,
    code_generated: localStorage.getItem("code_generated"),
    code_paie: code_paie,
    base: base,
    gain: gain,
    retenue: retenue,
    compte_comptable: compte_comptable,
  };
  try {
    const response = await axios.post(api_url + "lignepaie/", credentiel, config);
    console.log(response);
    toast.success("Ligne de paie ajouté");
    return response;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      toast.error("error server");
      console.log(error.response.data);
      console.log(error.response.message);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the
      // browser and an instance of
      // http.ClientRequest in node.js
      toast.error("error request");
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return error;
  }
}

export async function editLignePaie(data) {
  const credentiel = data;
  try {
    const response = await axios.put(api_url + "lignepaie/id/", credentiel, config);
    console.log(response);
    toast.success("mise à jour");
    return response;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      toast.error("error server");
      console.log(error.response.data);
      console.log(error.response.message);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the
      // browser and an instance of
      // http.ClientRequest in node.js
      toast.error("error request");
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return error;
  }
}
