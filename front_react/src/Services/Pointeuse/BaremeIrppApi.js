import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import axios from "axios";
import { getToken, getSession } from "./../../Utils/SessionUtils";

const api_url = process.env.REACT_APP_API_BASE_URL;
const config = {
  headers: { Authorization: `Bearer ${getToken()}` },
};

/**
 * get bareme irpp by code
 */
export async function getBaremeIrppByCode() {
  try {
    const response = await axios.get(api_url + "baremeirpp/" + getSession("code_generated"), config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * get bareme irpp by id
 */
export async function getBaremeIrppById(id) {
  try {
    const response = await axios.get(api_url + "baremeirpp/id/" + id, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * edit bareme irp with id
 * @param {*} code
 * @param {*} min
 * @param {*} max
 * @param {*} taux
 * @param {*} taux_sup
 * @param {*} id
 */
export async function editBaremeIrpp(code, min, max, taux, taux_sup, id) {
  const credentiel = {
    code: code,
    min: min,
    max: max,
    taux: taux,
    taux_sup: taux_sup,
    id: id,
  };

  try {
    const response = await axios.put(api_url + "baremeirpp/id/", credentiel, config);
    console.log(response);
    toast.success("mise à jour");
    return response;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      toast.error("error server");
      console.log(error.response.data);
      console.log(error.response.message);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the
      // browser and an instance of
      // http.ClientRequest in node.js
      toast.error("error request");
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return error;
  }
}

/**
 * add new barem irpp
 * @param {} code
 * @param {*} min
 * @param {*} max
 * @param {*} taux
 * @param {*} taux_sup
 * @param {*} id
 */
export async function addBaremeIrpp(code_generated, code, min, max, taux, taux_sup, id) {
  const credentiel = {
    code_generated: code_generated,
    code: code,
    min: min,
    max: max,
    taux: taux,
    taux_sup: taux_sup,
    id: id,
  };

  try {
    const response = await axios.post(api_url + "baremeirpp/", credentiel, config);
    console.log(response);
    toast.success("Barème irpp ajouté");
    return response;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      toast.error("error server");
      console.log(error.response.data);
      console.log(error.response.message);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the
      // browser and an instance of
      // http.ClientRequest in node.js
      toast.error("error request");
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return error;
  }
}
