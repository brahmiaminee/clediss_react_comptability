import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import { getToken, getSession } from "./../../Utils/SessionUtils";

const api_url = process.env.REACT_APP_API_BASE_URL;
const config = {
  headers: { Authorization: `Bearer ${getToken()}` },
};

export async function getAllSalaires() {
  try {
    const response = await axios.get(api_url + "salaire/code/" + getSession("code_generated"), config);

    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function getAllSalairesVirement(code) {
  try {
    const response = await axios.get(api_url + "salaire/codevirement/" + code, config);

    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function addSalaire(data, date, sequence) {
  const credentiel = {
    code_generated: localStorage.getItem("code_generated"),
    user_id: data.userId,
    code: sequence,
    cin: data.cin,
    nom: data.nom,
    rib: data.rib,
    banque: data.banque,
    salaire: data.montant,
    mois: date.split("-")[1],
    annee: date.split("-")[0],
  };
  try {
    const response = await axios.post(api_url + "salaire/", credentiel, config);
    console.log(response);
    //toast.success("ajouté");
    return response;
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      toast.error("error server");
      console.log(error.response.data);
      console.log(error.response.message);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the
      // browser and an instance of
      // http.ClientRequest in node.js
      toast.error("error request");
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
    console.log(error.config);
    return error;
  }
}

export async function deleteSalaireByCode(code) {
  try {
    const response = await axios.delete(api_url + "salaire/" + code, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}
