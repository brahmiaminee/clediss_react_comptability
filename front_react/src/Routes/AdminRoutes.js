import AnnonceList from "../components/Annonce/AnnonceList";
import Autorisation from "../components/Autorisation/Autorisation";
import BaremeIrpp from "../components/BaremeIrpp/BaremeIrpp";
import Chat from "../components/Chat/Chat";
import DemandeConges from "../components/Conges/DemandeConges";
import Holiday from "../components/Holiday/Holiday";
import NonValideConges from "../components/Conges/NonValideConges";
import ValideConges from "../components/Conges/ValideConges";
import Entreprise from "../components/Entreprise.js/Entreprise";
import AddClient from "../components/Facturation/Client/AddClient";
import ListClient from "../components/Facturation/Client/ListClient";
import AjoutBlAchat from "../components/Facturation/Document/Achat/BL/AjoutBlAchat";
import EditerBlAchat from "../components/Facturation/Document/Achat/BL/EditerBlAchat";
import ListBlAchat from "../components/Facturation/Document/Achat/BL/ListBlAchat";
import AjoutCommandeAchat from "../components/Facturation/Document/Achat/Commande/AjoutCommandeAchat";
import EditerCommandeAchat from "../components/Facturation/Document/Achat/Commande/EditerCommandeAchat";
import ListCommandeAchat from "../components/Facturation/Document/Achat/Commande/ListCommandeAchat";
import AjoutDevisAchat from "../components/Facturation/Document/Achat/Devis/AjoutDevisAchat";
import EditerDevisAchat from "../components/Facturation/Document/Achat/Devis/EditerDevisAchat";
import ListDevisAchat from "../components/Facturation/Document/Achat/Devis/ListDevisAchat";
import AjoutFactureAchat from "../components/Facturation/Document/Achat/Facture/AjoutFactureAchat";
import EditerFactureAchat from "../components/Facturation/Document/Achat/Facture/EditerFactureAchat";
import ListFactureAchat from "../components/Facturation/Document/Achat/Facture/ListFactureAchat";
import AjoutBlVente from "../components/Facturation/Document/Vente/BL/AjoutBlVente";
import EditerBlVente from "../components/Facturation/Document/Vente/BL/EditerBlVente";
import ListBlVente from "../components/Facturation/Document/Vente/BL/ListBlVente";
import AjoutCommandeVente from "../components/Facturation/Document/Vente/Commande/AjoutCommandeVente";
import EditerCommandeVente from "../components/Facturation/Document/Vente/Commande/EditerCommandeVente";
import ListCommandeVente from "../components/Facturation/Document/Vente/Commande/ListCommandeVente";
import AjoutDevisVente from "../components/Facturation/Document/Vente/Devis/AjoutDevisVente";
import EditerDevisVente from "../components/Facturation/Document/Vente/Devis/EditerDevisVente";
import ListDevisVente from "../components/Facturation/Document/Vente/Devis/ListDevisVente";
import AjoutFactureVente from "../components/Facturation/Document/Vente/Facture/AjoutFactureVente";
import EditerFactureVente from "../components/Facturation/Document/Vente/Facture/EditerFactureVente";
import ListFactureVente from "../components/Facturation/Document/Vente/Facture/ListFactureVente";
import AddFamille from "../components/Facturation/Famille/AddFamille";
import EditerFamille from "../components/Facturation/Famille/EditerFamille";
import ListFamille from "../components/Facturation/Famille/ListFamille";
import AddFournisseur from "../components/Facturation/Fournisseur/AddFournisseur";
import ListFournisseur from "../components/Facturation/Fournisseur/ListFournisseur";
import AddGamme from "../components/Facturation/Gamme/AddGamme";
import EditerGamme from "../components/Facturation/Gamme/EditerGamme";
import ListGamme from "../components/Facturation/Gamme/ListGamme";
import AddMarque from "../components/Facturation/Marque/AddMarque";
import EditerMarque from "../components/Facturation/Marque/EditerMarque";
import ListMarque from "../components/Facturation/Marque/ListMarque";
import Prefix from "../components/Facturation/Prefix/Prefix";
import AddProduct from "../components/Facturation/Produit/AddProduct";
import ProductList from "../components/Facturation/Produit/ProductList";
import ProductParams from "../components/Facturation/Produit/ProductParams";
import AddSousFamille from "../components/Facturation/SousFamille/AddSousFamille";
import EditerSousFamille from "../components/Facturation/SousFamille/EditerSousFamille";
import ListSousFamille from "../components/Facturation/SousFamille/ListSousFamille";
import Tax from "../components/Facturation/Tax/Tax";
import LignePaie from "../components/LignePaie/LignePaie";
import PointageMonth from "../components/Pointages/PointageMonth";
import PointageToday from "../components/Pointages/PointageToday";
import Profil from "../components/Profil/Profil";
import ProjectDetails from "../components/Projects/ProjectDetails";
import ProjectsList from "../components/Projects/ProjectsList";
import Support from "../components/Support/Support";
import TasksList from "../components/Tasks/TasksList";
import EditerPersonnel from "../components/Users/EditerPersonnel";
import EditUser from "../components/Users/EditUser";
import UsersList from "../components/Users/UsersList";
import VirementSalaire from "../components/Virement/VirementSalaire";
import DashboardLayout from "../layout/DashboardLayout";
import LoginLoyout from "../layout/LoginLayout";
import MainDashboard from "../pages/MainDashboard";
import SignIn from "../pages/SignIn";
import PrivateRoute from "../routes/PrivateRoute";
import PublicRoute from "../routes/PublicRoute";
import EditTax from "../components/Facturation/Tax/EditTax";
import EditProduct from "../components/Facturation/Produit/EditProduct";
import EditerClient from "../components/Facturation/Client/EditerClient";
import EditerFournisseur from "../components/Facturation/Fournisseur/EditerFournisseur";
import Blog from "../components/Blog/Blog";
import AnnonceDetail from "../components/Annonce/AnnonceDetail";
import EditerHoliday from "../components/Holiday/EditerHoliday";
import { Redirect } from "react-router";
import FilesList from "../components/files/FilesList";
import Messanger from "../components/Chat/Messanger";
import FilesMain from "../components/files/FilesMain";
import Encaissement from "../components/Facturation/Encaissement/AddEncaissement";
import AddEncaissement from "../components/Facturation/Encaissement/AddEncaissement";
import ListEncaissement from "../components/Facturation/Encaissement/ListEncaissement";
import EditEncaissement from "../components/Facturation/Encaissement/EditEncaissement";
import UploadDecharge from "../components/Facturation/Document/UploadDecharge";
import ClientActivity from "../components/Facturation/ClientActivity/ClientActivity";
import BillParams from "../components/Facturation/invoiceReports/BillParams";
import AddAbonnement from "../components/Abonnement/AddAbonnement";
import ListAbonnement from "../components/Abonnement/ListAbonnement";
import EditAbonnement from "../components/Abonnement/EditAbonnement";
import EditerFactureAbonnement from "../components/Abonnement/EditerFactureAbonnement";
import AddVaccin from "../components/Vaccination/AddVaccin";
import ListVaccin from "../components/Vaccination/ListVaccin";
import ListTeletravail from "../components/teletravail/ListTeletravail";
import EditerVaccin from "../components/Vaccination/EditerVaccin";
import PrintConge from "../components/Conges/PrintConge";
import Objectif from "../components/Objectif/Objectif";
import AddPaiement from "../components/Facturation/Paiement/AddPaiement";
import ListPaiement from "../components/Facturation/Paiement/ListPaiement";
import EditerPaiement from "../components/Facturation/Paiement/EditerPaiement";
import KanbanTasks from "../components/Tasks/KanbanTasks";
import CongeCalendar from "../components/Conges/CongeCalendar";
import ProjetCalendar from "../components/Projects/ProjetCalendar";
import ListFormations from "../components/Formations/ListFormations";
import ListAcompte from "../components/Acompte/ListAcompte";
import ListDepartement from "../components/Departement/ListDepartement";
import AddDepartement from "../components/Departement/AddDepartement";
import EditDepartement from "../components/Departement/EditDepartement";
import ListDemandeMaintenance from "../components/Maintenance/ListDemandeMaintenance";
import ListDemandeMateriel from "../components/Materiel/ListDemandeMateriel";
import DetailsMateriel from "../components/Materiel/DetailsMateriel";
import DetailTask from "../components/Tasks/DetailTask";
import ListSalaire from "../components/Virement/ListSalaire";
import EditerVirement from "../components/Virement/EditerVirement";
import Cheque from "../components/Cheque/Cheque";

// eslint-disable-next-line import/no-anonymous-default-export
export default [
  <PrivateRoute path="/admin/users" exact layout={DashboardLayout} component={UsersList} />,
  <PrivateRoute path="/admin/users/:id" layout={DashboardLayout} component={EditUser} />,
  <PrivateRoute path="/admin/chat/:id" layout={DashboardLayout} component={Chat} />,
  <PrivateRoute path="/admin/messanger" layout={DashboardLayout} component={Messanger} />,
  <PrivateRoute path="/admin/userspersonnel/:id" layout={DashboardLayout} component={EditerPersonnel} />,
  <PrivateRoute path="/admin/profil" exact layout={DashboardLayout} component={Profil} />,
  <PrivateRoute path="/admin/tasks" exact layout={DashboardLayout} component={TasksList} />,
  <PrivateRoute path="/admin/tasks/:id" layout={DashboardLayout} component={DetailTask} />,
  <PrivateRoute path="/admin/kanban" layout={DashboardLayout} component={KanbanTasks} />,
  <PrivateRoute path="/admin/projects" layout={DashboardLayout} component={ProjectsList} />,
  <PrivateRoute path="/admin/projectdetail/:id" layout={DashboardLayout} component={ProjectDetails} />,
  <PrivateRoute path="/admin/calendrierprojet" layout={DashboardLayout} component={ProjetCalendar} />,
  <PrivateRoute path="/admin/jourpointage" layout={DashboardLayout} component={PointageToday} />,
  <PrivateRoute path="/admin/moispointage" layout={DashboardLayout} component={PointageMonth} />,
  <PrivateRoute path="/admin/autorisation" layout={DashboardLayout} component={Autorisation} />,
  <PrivateRoute path="/admin/annonce" exact layout={DashboardLayout} component={AnnonceList} />,
  <PrivateRoute path="/admin/annoncedetail/:id" layout={DashboardLayout} component={AnnonceDetail} />,
  <PrivateRoute path="/admin/demandeconge" layout={DashboardLayout} component={DemandeConges} />,
  <PrivateRoute path="/admin/congevalide" layout={DashboardLayout} component={ValideConges} />,
  <PrivateRoute path="/admin/congenonvalide" layout={DashboardLayout} component={NonValideConges} />,
  <PrivateRoute path="/admin/fileconge" layout={DashboardLayout} component={FilesMain} />,
  <PrivateRoute path="/admin/holiday" layout={DashboardLayout} component={Holiday} />,
  <PrivateRoute path="/admin/editholiday/:id" layout={DashboardLayout} component={EditerHoliday} />,
  <PrivateRoute path="/admin/calendarconge" layout={DashboardLayout} component={CongeCalendar} />,
  <PrivateRoute path="/admin/support" layout={DashboardLayout} component={Support} />,
  <PrivateRoute path="/admin/blog" layout={DashboardLayout} component={Blog} />,
  <PrivateRoute path="/admin/entreprise" layout={DashboardLayout} component={Entreprise} />,
  <PrivateRoute path="/admin/baremirpp" layout={DashboardLayout} component={BaremeIrpp} />,
  <PrivateRoute path="/admin/lignepaie" layout={DashboardLayout} component={LignePaie} />,
  <PrivateRoute path="/admin/virementsalaire" layout={DashboardLayout} component={VirementSalaire} />,
  <PrivateRoute path="/admin/paramfacture" layout={DashboardLayout} component={BillParams} />,
  <PrivateRoute path="/admin/ajoutfacturevente" layout={DashboardLayout} component={AjoutFactureVente} />,
  <PrivateRoute path="/admin/editerfacturevente/:id" layout={DashboardLayout} component={EditerFactureVente} />,
  <PrivateRoute path="/admin/facturevente" layout={DashboardLayout} component={ListFactureVente} />,
  <PrivateRoute path="/admin/blvente" layout={DashboardLayout} component={ListBlVente} />,
  <PrivateRoute path="/admin/ajoutblvente" layout={DashboardLayout} component={AjoutBlVente} />,
  <PrivateRoute path="/admin/editerblvente/:id" layout={DashboardLayout} component={EditerBlVente} />,
  <PrivateRoute path="/admin/devisvente" layout={DashboardLayout} component={ListDevisVente} />,
  <PrivateRoute path="/admin/ajoutdevisvente" layout={DashboardLayout} component={AjoutDevisVente} />,
  <PrivateRoute path="/admin/editerdevisvente/:id" layout={DashboardLayout} component={EditerDevisVente} />,
  <PrivateRoute path="/admin/commandevente" layout={DashboardLayout} component={ListCommandeVente} />,
  <PrivateRoute path="/admin/ajoutcommandevente" layout={DashboardLayout} component={AjoutCommandeVente} />,
  <PrivateRoute path="/admin/editercommandevente/:id" layout={DashboardLayout} component={EditerCommandeVente} />,
  <PrivateRoute path="/admin/ajoutfactureachat" layout={DashboardLayout} component={AjoutFactureAchat} />,
  <PrivateRoute path="/admin/editerfactureachat/:id" layout={DashboardLayout} component={EditerFactureAchat} />,
  <PrivateRoute path="/admin/factureachat" layout={DashboardLayout} component={ListFactureAchat} />,
  <PrivateRoute path="/admin/blachat" layout={DashboardLayout} component={ListBlAchat} />,
  <PrivateRoute path="/admin/ajoutblachat" layout={DashboardLayout} component={AjoutBlAchat} />,
  <PrivateRoute path="/admin/editerblachat/:id" layout={DashboardLayout} component={EditerBlAchat} />,
  <PrivateRoute path="/admin/devisachat" layout={DashboardLayout} component={ListDevisAchat} />,
  <PrivateRoute path="/admin/ajoutdevisachat" layout={DashboardLayout} component={AjoutDevisAchat} />,
  <PrivateRoute path="/admin/editerdevisachat/:id" layout={DashboardLayout} component={EditerDevisAchat} />,
  <PrivateRoute path="/admin/commandeachat" layout={DashboardLayout} component={ListCommandeAchat} />,
  <PrivateRoute path="/admin/ajoutcommandeachat" layout={DashboardLayout} component={AjoutCommandeAchat} />,
  <PrivateRoute path="/admin/editercommandeachat/:id" layout={DashboardLayout} component={EditerCommandeAchat} />,
  <PrivateRoute path="/admin/produit" exact layout={DashboardLayout} component={ProductList} />,
  <PrivateRoute path="/admin/produit/:id" layout={DashboardLayout} component={EditProduct} />,
  <PrivateRoute path="/admin/addproduct" layout={DashboardLayout} component={AddProduct} />,
  <PrivateRoute path="/admin/fournisseur" exact layout={DashboardLayout} component={ListFournisseur} />,
  <PrivateRoute path="/admin/fournisseur/:id" layout={DashboardLayout} component={EditerFournisseur} />,
  <PrivateRoute path="/admin/addfournisseur" layout={DashboardLayout} component={AddFournisseur} />,
  <PrivateRoute path="/admin/client" exact layout={DashboardLayout} component={ListClient} />,
  <PrivateRoute path="/admin/client/:id" layout={DashboardLayout} component={EditerClient} />,
  <PrivateRoute path="/admin/addclient" layout={DashboardLayout} component={AddClient} />,
  <PrivateRoute path="/admin/tax" exact layout={DashboardLayout} component={Tax} />,
  <PrivateRoute path="/admin/tax/:id" layout={DashboardLayout} component={EditTax} />,
  <PrivateRoute path="/admin/prefix" layout={DashboardLayout} component={Prefix} />,
  <PrivateRoute path="/admin/encaissement" layout={DashboardLayout} component={ListEncaissement} />,
  <PrivateRoute path="/admin/addencaissement" layout={DashboardLayout} component={AddEncaissement} />,
  <PrivateRoute path="/admin/facturation/editencaissement/:id" layout={DashboardLayout} component={EditEncaissement} />,
  <PrivateRoute path="/admin/facturation/marque" layout={DashboardLayout} component={ListMarque}></PrivateRoute>,
  <PrivateRoute path="/admin/facturation/addmarque" layout={DashboardLayout} component={AddMarque}></PrivateRoute>,
  <PrivateRoute
    path="/admin/facturation/editmarque/:id"
    layout={DashboardLayout}
    component={EditerMarque}
  ></PrivateRoute>,
  <PrivateRoute path="/admin/facturation/gamme" layout={DashboardLayout} component={ListGamme}></PrivateRoute>,
  <PrivateRoute path="/admin/facturation/addgamme" layout={DashboardLayout} component={AddGamme}></PrivateRoute>,
  <PrivateRoute
    path="/admin/facturation/editgamme/:id"
    layout={DashboardLayout}
    component={EditerGamme}
  ></PrivateRoute>,
  <PrivateRoute path="/admin/facturation/addfamille" layout={DashboardLayout} component={AddFamille}></PrivateRoute>,
  <PrivateRoute path="/admin/facturation/famille" layout={DashboardLayout} component={ListFamille}></PrivateRoute>,
  <PrivateRoute
    path="/admin/facturation/editfamille/:id"
    layout={DashboardLayout}
    component={EditerFamille}
  ></PrivateRoute>,
  <PrivateRoute
    path="/admin/facturation/sousfamille"
    layout={DashboardLayout}
    component={ListSousFamille}
  ></PrivateRoute>,
  <PrivateRoute
    path="/admin/facturation/addsousfamille"
    layout={DashboardLayout}
    component={AddSousFamille}
  ></PrivateRoute>,
  <PrivateRoute
    path="/admin/facturation/editsousfamille/:id"
    layout={DashboardLayout}
    component={EditerSousFamille}
  ></PrivateRoute>,
  <PrivateRoute
    path="/admin/facturation/productparam"
    layout={DashboardLayout}
    component={ProductParams}
  ></PrivateRoute>,
  <PrivateRoute
    path="/admin/facturation/uploaddecharge/:id"
    layout={DashboardLayout}
    component={UploadDecharge}
  ></PrivateRoute>,
  <PrivateRoute
    path="/admin/facturation/clientactivity/:id"
    layout={DashboardLayout}
    component={ClientActivity}
  ></PrivateRoute>,
  <PrivateRoute path="/admin/addabonnement" layout={DashboardLayout} component={AddAbonnement}></PrivateRoute>,
  <PrivateRoute path="/admin/listabonnement" layout={DashboardLayout} component={ListAbonnement}></PrivateRoute>,
  <PrivateRoute
    path="/admin/facturation/editabonnement/:id"
    layout={DashboardLayout}
    component={EditAbonnement}
  ></PrivateRoute>,
  <PrivateRoute
    path="/admin/facturation/editfactureabonnement/:id"
    layout={DashboardLayout}
    component={EditerFactureAbonnement}
  ></PrivateRoute>,
  <PrivateRoute path="/admin/vaccin" layout={DashboardLayout} component={ListVaccin}></PrivateRoute>,
  <PrivateRoute path="/admin/addvaccin" layout={DashboardLayout} component={AddVaccin}></PrivateRoute>,
  <PrivateRoute path="/admin/teletravail" layout={DashboardLayout} component={ListTeletravail}></PrivateRoute>,
  <PrivateRoute path="/admin/editteletravail/:id" layout={DashboardLayout} component={EditerVaccin}></PrivateRoute>,
  <PrivateRoute path="/admin/congeprint/:id" layout={DashboardLayout} component={PrintConge}></PrivateRoute>,
  <PrivateRoute path="/admin/objectif" layout={DashboardLayout} component={Objectif}></PrivateRoute>,
  <PrivateRoute path="/admin/addpaiement" layout={DashboardLayout} component={AddPaiement}></PrivateRoute>,
  <PrivateRoute path="/admin/paiement" layout={DashboardLayout} component={ListPaiement}></PrivateRoute>,
  <PrivateRoute
    path="/admin/facturation/editpaiement/:id"
    layout={DashboardLayout}
    component={EditerPaiement}
  ></PrivateRoute>,
  <PrivateRoute path="/admin/formations" layout={DashboardLayout} component={ListFormations}></PrivateRoute>,
  <PrivateRoute path="/admin/acomptesalaire" layout={DashboardLayout} component={ListAcompte}></PrivateRoute>,
  <PrivateRoute path="/admin/departements" layout={DashboardLayout} component={ListDepartement}></PrivateRoute>,
  <PrivateRoute path="/admin/ajouterdepartement" layout={DashboardLayout} component={AddDepartement}></PrivateRoute>,
  <PrivateRoute
    path="/admin/editerdepartement/:id"
    layout={DashboardLayout}
    component={EditDepartement}
  ></PrivateRoute>,
  <PrivateRoute path="/admin/materiel" exact layout={DashboardLayout} component={ListDemandeMateriel}></PrivateRoute>,
  <PrivateRoute path="/admin/maintenance" layout={DashboardLayout} component={ListDemandeMaintenance}></PrivateRoute>,
  <PrivateRoute path="/admin/materiel/:id" layout={DashboardLayout} component={DetailsMateriel}></PrivateRoute>,
  <PrivateRoute path="/admin/salaire" exact layout={DashboardLayout} component={ListSalaire}></PrivateRoute>,
  <PrivateRoute path="/admin/salaire/:id" exact layout={DashboardLayout} component={EditerVirement}></PrivateRoute>,
  <PrivateRoute path="/admin/cheque" exact layout={DashboardLayout} component={Cheque}></PrivateRoute>,
];
