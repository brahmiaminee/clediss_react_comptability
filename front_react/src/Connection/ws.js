const io = require("socket.io-client");
module.exports = io(process.env.REACT_APP_API);
