import React from "react";
import { Link } from "react-router-dom";
import Footer from "../Components/shared/Footer";
import Header from "../Components/shared/Header";
import Preloader from "../Components/shared/Preloader";
import Sidebar from "../Components/shared/Sidebar";

function DashboardLayout(props) {
  return (
    <div>
      {/* Switcher */}
      {/* <div className="switcher-wrapper">
        <div className="demo_changer">
          <div className="demo-icon bg_dark">
            <i className="fa fa-cog fa-spin  text_primary" />
          </div>
          <div className="form_holder sidebar-right1">
            <div className="row">
              <div className="predefined_styles">
                <div className="swichermainleft text-center">
                  <div className="p-3">
                    <a href="index.html" className="btn ripple btn-primary btn-block mt-0">
                      View Demo
                    </a>
                    <a
                      href="https://themeforest.net/user/spruko/portfolio"
                      className="btn ripple btn-secondary btn-block"
                    >
                      Buy Now
                    </a>
                    <a href="https://themeforest.net/user/spruko/portfolio" className="btn ripple btn-red btn-block">
                      Our Portfolio
                    </a>
                  </div>
                </div>
                <div className="swichermainleft mb-4">
                  <h4>Navigation Style</h4>
                  <div className="pl-3 pr-3 pt-3">
                    <a className="btn ripple btn-success btn-block" href="verticalmenu.html">
                      {" "}
                      Leftmenu
                    </a>
                    <a className="btn ripple btn-danger btn-block" href="horizontal.html">
                      {" "}
                      Horizontal{" "}
                    </a>
                  </div>
                </div>
                <div className="swichermainleft">
                  <h4>Theme Layout</h4>
                  <div className="switch_section d-flex my-4">
                    <ul className="switch-buttons row">
                      <li className="col-md-6 mb-0">
                        <button type="button" id="background-left1" className="bg-left1 wscolorcode1 button-image" />
                        <span className="d-block">Light Theme</span>
                      </li>
                      <li className="col-md-6 mb-0">
                        <button type="button" id="background-left2" className="bg-left2 wscolorcode1 button-image" />
                        <span className="d-block">Dark Theme</span>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="swichermainleft">
                  <h4>Header Styles Mode</h4>
                  <div className="switch_section d-flex my-4">
                    <ul className="switch-buttons row">
                      <li className="col-md-6 light-bg">
                        <button type="button" id="background1" className="bg1 wscolorcode1 button-image" />
                        <span className="d-block">Light Header</span>
                      </li>
                      <li className="col-md-6">
                        <button type="button" id="background2" className="bg2 wscolorcode1 button-image" />
                        <span className="d-block">Color Header</span>
                      </li>
                      <li className="col-md-6 d-block mx-auto dark-bg">
                        <button type="button" id="background3" className="bg3 wscolorcode1 button-image" />
                        <span className="d-block">Dark Header</span>
                      </li>
                      <li className="col-md-6 d-block mx-auto">
                        <button type="button" id="background11" className="bg8 wscolorcode1 button-image" />
                        <span className="d-block">Gradient Header</span>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="swichermainleft">
                  <h4>Leftmenu Styles Mode</h4>
                  <div className="switch_section d-flex my-4">
                    <ul className="switch-buttons row">
                      <li className="col-md-6 mb-4">
                        <button type="button" id="background4" className="bg4 wscolorcode1 button-image" />
                        <span className="d-block">Light Menu</span>
                      </li>
                      <li className="col-md-6 mb-4">
                        <button type="button" id="background5" className="bg5 wscolorcode1 button-image" />
                        <span className="d-block">Color Menu</span>
                      </li>
                      <li className="col-md-6 mb-0 d-block mx-auto dark-bgmenu">
                        <button type="button" id="background6" className="bg6 wscolorcode1 button-image" />
                        <span className="d-block">Dark Menu</span>
                      </li>
                      <li className="col-md-6 mb-0 d-block mx-auto">
                        <button type="button" id="background10" className="bg7 wscolorcode1 button-image" />
                        <span className="d-block">Gradient Menu</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> */}
      {/* End Switcher */}
      {/*-Global-loader*/}
      {/* <Preloader></Preloader> */}
      <div className="page">
        <div className="page-main">
          {/*aside open*/}
          <Sidebar></Sidebar>
          {/*aside closed*/}
          <div className="app-content main-content">
            <div className="side-app">
              {/*app header*/}
              <Header></Header>
              {/*/app header*/}
              {/*Page header*/}
              {/* <div className="m-2"> */}
              <React.Fragment>{props.children}</React.Fragment>
              {/* </div> */}
            </div>
          </div>
          {/* end app-content*/}
        </div>
        {/*Footer*/}
        <Footer></Footer>
        {/* End Footer*/}
        {/*Sidebar-right*/}
        <div className="sidebar sidebar-right sidebar-animate">
          <div className="card-header border-bottom pb-5">
            <h4 className="card-title">Notifications </h4>
            <div className="card-options">
              <a
                href="#"
                className="btn btn-sm btn-icon btn-light  text-primary"
                data-toggle="sidebar-right"
                data-target=".sidebar-right"
              >
                <i className="feather feather-x" />{" "}
              </a>
            </div>
          </div>
          <div>
            <div className="list-group-item  align-items-center border-0">
              <div className="d-flex">
                <span
                  className="avatar avatar-lg brround mr-3"
                  style={{ backgroundImage: "url(assets/images/users/4.jpg)" }}
                />
                <div className="mt-1">
                  <a href="#" className="font-weight-semibold fs-16">
                    Liam <span className="text-muted font-weight-normal">Sent Message</span>
                  </a>
                  <span className="clearfix" />
                  <span className="text-muted fs-13 ml-auto">
                    <i className="mdi mdi-clock text-muted mr-1" />
                    30 mins ago
                  </span>
                </div>
                <div className="ml-auto">
                  <Link
                    to="#"
                    className="mr-0 option-dots"
                    data-toggle="dropdown"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <span className="feather feather-more-horizontal" />
                  </Link>
                  <ul className="dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                      <a href="#">
                        <i className="feather feather-eye mr-2" />
                        View
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-plus-circle mr-2" />
                        Add
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-trash-2 mr-2" />
                        Remove
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-settings mr-2" />
                        More
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="list-group-item  align-items-center  border-bottom">
              <div className="d-flex">
                <span
                  className="avatar avatar-lg brround mr-3"
                  style={{ backgroundImage: "url(assets/images/users/10.jpg)" }}
                />
                <div className="mt-1">
                  <a href="#" className="font-weight-semibold fs-16">
                    Paul<span className="text-muted font-weight-normal"> commented on you post</span>
                  </a>
                  <span className="clearfix" />
                  <span className="text-muted fs-13 ml-auto">
                    <i className="mdi mdi-clock text-muted mr-1" />1 hour ago
                  </span>
                </div>
                <div className="ml-auto">
                  <Link
                    to="#"
                    className="mr-0 option-dots"
                    data-toggle="dropdown"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <span className="feather feather-more-horizontal" />
                  </Link>
                  <ul className="dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                      <a href="#">
                        <i className="feather feather-eye mr-2" />
                        View
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-plus-circle mr-2" />
                        Add
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-trash-2 mr-2" />
                        Remove
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-settings mr-2" />
                        More
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="list-group-item  align-items-center  border-bottom">
              <div className="d-flex">
                <span className="avatar avatar-lg brround mr-3 bg-pink-transparent">
                  <span className="feather feather-shopping-cart" />
                </span>
                <div className="mt-1">
                  <a href="#" className="font-weight-semibold fs-16">
                    James<span className="text-muted font-weight-normal"> Order Placed</span>
                  </a>
                  <span className="clearfix" />
                  <span className="text-muted fs-13 ml-auto">
                    <i className="mdi mdi-clock text-muted mr-1" />1 day ago
                  </span>
                </div>
                <div className="ml-auto">
                  <Link
                    to="#"
                    className="mr-0 option-dots"
                    data-toggle="dropdown"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <span className="feather feather-more-horizontal" />
                  </Link>
                  <ul className="dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                      <a href="#">
                        <i className="feather feather-eye mr-2" />
                        View
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-plus-circle mr-2" />
                        Add
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-trash-2 mr-2" />
                        Remove
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-settings mr-2" />
                        More
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="list-group-item  align-items-center  border-bottom">
              <div className="d-flex">
                <span
                  className="avatar avatar-lg brround mr-3"
                  style={{ backgroundImage: "url(assets/images/users/9.jpg)" }}
                >
                  <span className="avatar-status bg-green" />
                </span>
                <div className="mt-1">
                  <a href="#" className="font-weight-semibold fs-16">
                    Diane<span className="text-muted font-weight-normal"> New Message Received</span>
                  </a>
                  <span className="clearfix" />
                  <span className="text-muted fs-13 ml-auto">
                    <i className="mdi mdi-clock text-muted mr-1" />1 day ago
                  </span>
                </div>
                <div className="ml-auto">
                  <Link
                    to="#"
                    className="mr-0 option-dots"
                    data-toggle="dropdown"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <span className="feather feather-more-horizontal" />
                  </Link>
                  <ul className="dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                      <a href="#">
                        <i className="feather feather-eye mr-2" />
                        View
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-plus-circle mr-2" />
                        Add
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-trash-2 mr-2" />
                        Remove
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-settings mr-2" />
                        More
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="list-group-item  align-items-center  border-bottom">
              <div className="d-flex">
                <span
                  className="avatar avatar-lg brround mr-3"
                  style={{ backgroundImage: "url(assets/images/users/5.jpg)" }}
                >
                  <span className="avatar-status bg-muted" />
                </span>
                <div className="mt-1">
                  <a href="#" className="font-weight-semibold fs-16">
                    Vinny<span className="text-muted font-weight-normal"> shared your post</span>
                  </a>
                  <span className="clearfix" />
                  <span className="text-muted fs-13 ml-auto">
                    <i className="mdi mdi-clock text-muted mr-1" />2 days ago
                  </span>
                </div>
                <div className="ml-auto">
                  <Link
                    to="#"
                    className="mr-0 option-dots"
                    data-toggle="dropdown"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <span className="feather feather-more-horizontal" />
                  </Link>
                  <ul className="dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                      <a href="#">
                        <i className="feather feather-eye mr-2" />
                        View
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-plus-circle mr-2" />
                        Add
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-trash-2 mr-2" />
                        Remove
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-settings mr-2" />
                        More
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="list-group-item  align-items-center  border-bottom">
              <div className="d-flex">
                <span className="avatar avatar-lg brround mr-3 bg-primary-transparent">M</span>
                <div className="mt-1">
                  <a href="#" className="font-weight-semibold fs-16">
                    Mack<span className="text-muted font-weight-normal"> your admin lanuched</span>
                  </a>
                  <span className="clearfix" />
                  <span className="text-muted fs-13 ml-auto">
                    <i className="mdi mdi-clock text-muted mr-1" />1 week ago
                  </span>
                </div>
                <div className="ml-auto">
                  <Link
                    to="#"
                    className="mr-0 option-dots"
                    data-toggle="dropdown"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <span className="feather feather-more-horizontal" />
                  </Link>
                  <ul className="dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                      <a href="#">
                        <i className="feather feather-eye mr-2" />
                        View
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-plus-circle mr-2" />
                        Add
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-trash-2 mr-2" />
                        Remove
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-settings mr-2" />
                        More
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="list-group-item  align-items-center  border-bottom">
              <div className="d-flex">
                <span
                  className="avatar avatar-lg brround mr-3"
                  style={{ backgroundImage: "url(assets/images/users/12.jpg)" }}
                >
                  <span className="avatar-status bg-green" />
                </span>
                <div className="mt-1">
                  <a href="#" className="font-weight-semibold fs-16">
                    Vinny<span className="text-muted font-weight-normal"> shared your post</span>
                  </a>
                  <span className="clearfix" />
                  <span className="text-muted fs-13 ml-auto">
                    <i className="mdi mdi-clock text-muted mr-1" />
                    04 Jan 2021 1:56 Am
                  </span>
                </div>
                <div className="ml-auto">
                  <Link
                    to="#"
                    className="mr-0 option-dots"
                    data-toggle="dropdown"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <span className="feather feather-more-horizontal" />
                  </Link>
                  <ul className="dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                      <a href="#">
                        <i className="feather feather-eye mr-2" />
                        View
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-plus-circle mr-2" />
                        Add
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-trash-2 mr-2" />
                        Remove
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-settings mr-2" />
                        More
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="list-group-item  align-items-center  border-bottom">
              <div className="d-flex">
                <span
                  className="avatar avatar-lg brround mr-3"
                  style={{ backgroundImage: "url(assets/images/users/8.jpg)" }}
                >
                  {" "}
                </span>
                <div className="mt-1">
                  <a href="#" className="font-weight-semibold fs-16">
                    Anna<span className="text-muted font-weight-normal"> likes your post</span>
                  </a>
                  <span className="clearfix" />
                  <span className="text-muted fs-13 ml-auto">
                    <i className="mdi mdi-clock text-muted mr-1" />
                    25 Dec 2020 11:25 Am
                  </span>
                </div>
                <div className="ml-auto">
                  <Link
                    to="#"
                    className="mr-0 option-dots"
                    data-toggle="dropdown"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <span className="feather feather-more-horizontal" />
                  </Link>
                  <ul className="dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                      <a href="#">
                        <i className="feather feather-eye mr-2" />
                        View
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-plus-circle mr-2" />
                        Add
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-trash-2 mr-2" />
                        Remove
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-settings mr-2" />
                        More
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="list-group-item  align-items-center  border-bottom">
              <div className="d-flex">
                <span
                  className="avatar avatar-lg brround mr-3"
                  style={{ backgroundImage: "url(assets/images/users/14.jpg)" }}
                >
                  {" "}
                </span>
                <div className="mt-1">
                  <a href="#" className="font-weight-semibold fs-16">
                    Kimberly<span className="text-muted font-weight-normal"> Completed one task</span>
                  </a>
                  <span className="clearfix" />
                  <span className="text-muted fs-13 ml-auto">
                    <i className="mdi mdi-clock text-muted mr-1" />
                    24 Dec 2020 9:30 Pm
                  </span>
                </div>
                <div className="ml-auto">
                  <Link
                    to="#"
                    className="mr-0 option-dots"
                    data-toggle="dropdown"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <span className="feather feather-more-horizontal" />
                  </Link>
                  <ul className="dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                      <a href="#">
                        <i className="feather feather-eye mr-2" />
                        View
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-plus-circle mr-2" />
                        Add
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-trash-2 mr-2" />
                        Remove
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-settings mr-2" />
                        More
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="list-group-item  align-items-center  border-bottom">
              <div className="d-flex">
                <span
                  className="avatar avatar-lg brround mr-3"
                  style={{ backgroundImage: "url(assets/images/users/3.jpg)" }}
                >
                  {" "}
                </span>
                <div className="mt-1">
                  <a href="#" className="font-weight-semibold fs-16">
                    Rina<span className="text-muted font-weight-normal"> your account has Updated</span>
                  </a>
                  <span className="clearfix" />
                  <span className="text-muted fs-13 ml-auto">
                    <i className="mdi mdi-clock text-muted mr-1" />
                    28 Nov 2020 7:16 Am
                  </span>
                </div>
                <div className="ml-auto">
                  <Link
                    to="#"
                    className="mr-0 option-dots"
                    data-toggle="dropdown"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <span className="feather feather-more-horizontal" />
                  </Link>
                  <ul className="dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                      <a href="#">
                        <i className="feather feather-eye mr-2" />
                        View
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-plus-circle mr-2" />
                        Add
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-trash-2 mr-2" />
                        Remove
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-settings mr-2" />
                        More
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="list-group-item  align-items-center  border-bottom">
              <div className="d-flex">
                <span className="avatar avatar-lg brround mr-3 bg-success-transparent">J</span>
                <div className="mt-1">
                  <a href="#" className="font-weight-semibold fs-16">
                    Julia<span className="text-muted font-weight-normal"> Prepare for Presentation</span>
                  </a>
                  <span className="clearfix" />
                  <span className="text-muted fs-13 ml-auto">
                    <i className="mdi mdi-clock text-muted mr-1" />
                    18 Nov 2020 11:55 Am
                  </span>
                </div>
                <div className="ml-auto">
                  <Link
                    to="#"
                    className="mr-0 option-dots"
                    data-toggle="dropdown"
                    role="button"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <span className="feather feather-more-horizontal" />
                  </Link>
                  <ul className="dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                      <a href="#">
                        <i className="feather feather-eye mr-2" />
                        View
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-plus-circle mr-2" />
                        Add
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-trash-2 mr-2" />
                        Remove
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="feather feather-settings mr-2" />
                        More
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*/Sidebar-right*/}
        {/*Clock-IN Modal */}
        <div className="modal fade" id="clockinmodal">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">
                  <span className="feather feather-clock  mr-1" />
                  Clock In
                </h5>
                <button className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="countdowntimer">
                  <span id="clocktimer" className="border-0" />
                </div>
                <div className="form-group">
                  <label className="form-label">Note:</label>
                  <textarea className="form-control" rows={3} defaultValue={"Some text here..."} />
                </div>
              </div>
              <div className="modal-footer">
                <button className="btn btn-outline-primary" data-dismiss="modal">
                  Close
                </button>
                <button className="btn btn-primary">Clock In</button>
              </div>
            </div>
          </div>
        </div>
        {/* End Clock-IN Modal  */}
        {/*Change password Modal */}
        <div className="modal fade" id="changepasswordnmodal">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Change Password</h5>
                <button className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="form-group">
                  <label className="form-label">New Password</label>
                  <input type="password" className="form-control" placeholder="password" defaultValue />
                </div>
                <div className="form-group">
                  <label className="form-label">Confirm New Password</label>
                  <input type="password" className="form-control" placeholder="password" defaultValue />
                </div>
              </div>
              <div className="modal-footer">
                <a href="#" className="btn btn-outline-primary" data-dismiss="modal">
                  Close
                </a>
                <a href="#" className="btn btn-primary">
                  Confirm
                </a>
              </div>
            </div>
          </div>
        </div>
        {/* End Change password Modal  */}
      </div>
    </div>
  );
}

export default DashboardLayout;
