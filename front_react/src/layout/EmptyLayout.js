import React from "react";

function EmptyLayout(props) {
  return <React.Fragment>{props.children}</React.Fragment>;
}

export default EmptyLayout;
