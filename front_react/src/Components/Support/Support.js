import React from "react";

function Support() {
  return (
    <div className="page-wrapper">
      <div className="content m-4">
        <div className="container">
          {/* Card */}
          <h4 className="page-title">Support</h4>
          <div className="row">
            <object
              type="text/html"
              data="https://tawk.to/chat/5f296b929b7cf26e73c1f35e/default"
              width="100%"
              height="700px"
              style={{ overflow: "auto" }}
            ></object>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Support;
