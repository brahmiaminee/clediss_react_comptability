import React from "react";

function CustomSyncLoader() {
  return (
    <div className="dimmer active">
      <div className="spinner4">
        <div className="bounce1" />
        <div className="bounce2" />
        <div className="bounce3" />
      </div>
    </div>
  );
}

export default CustomSyncLoader;
