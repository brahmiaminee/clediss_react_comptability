import React from "react";
import { Link } from "react-router-dom";

function ErrorCustum() {
  return (
    <div className="text-center">
      <div className="fs-100  mb-5 text-primary h1">oops!</div>
      <h1 className="h3  mb-3 font-weight-semibold">Error 500: Internal Server Error</h1>
      <p className="h5 font-weight-normal mb-7 leading-normal">
        You may have mistyped the address or the page may have moved.
      </p>
      <Link className="btn btn-primary" to="#" onClick={() => window.location.reload(false)}>
        <i className="fe fe-rotate-ccw mr-1" />
        Rafraîchir
      </Link>
    </div>
  );
}

export default ErrorCustum;
