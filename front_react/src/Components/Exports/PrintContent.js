import React from "react";
import { Link } from "react-router-dom";

function PrintContent() {
  return (
    <Link
      to="#"
      className="action-btns"
      data-toggle="tooltip"
      data-placement="top"
      title="pdf"
      onClick={() => window.print()}
    >
      <i className="feather feather-printer text-primary" />
    </Link>
  );
}

export default PrintContent;
