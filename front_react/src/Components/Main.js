import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

function Main() {
  const [date, setDate] = useState(new Date());
  const { t, i18n } = useTranslation();

  function refreshClock() {
    setDate(new Date());
  }

  useEffect(() => {
    const timerId = setInterval(refreshClock, 1000);
    return function cleanup() {
      clearInterval(timerId);
    };
  }, []);
  const getData = () => {};
  return <>this is the main page</>;
}

export default Main;
