import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { postSettings } from "../../Services/Pointeuse/SettingsApi";
import { addCode } from "../../Services/Pointeuse/UsersApi";
import { useAlert } from "react-alert";
import { hasNumber, hasSymbole, hasUperCase } from "../../Utils/CodeGeneratedUtils";

function WizardConfig() {
  const history = useHistory();
  const alert = useAlert();
  const [code, setcode] = useState(null);
  const [nbHoureWork, setnbHoureWork] = useState(null);
  const [nbJourWorkWeek, setnbJourWorkWeek] = useState(null);
  const [totalConge, settotalConge] = useState(null);
  const [startTime, setstartTime] = useState(null);
  const [endTime, setendTime] = useState(null);
  const [breakTime, setbreakTime] = useState(null);
  const [retardDay, setretardDay] = useState(null);
  const [retardMonth, setretardMonth] = useState(null);

  const [isLength, setisLength] = useState(true);
  const [isUpercase, setisUpercase] = useState(true);
  const [isNumber, setisNumber] = useState(true);
  const [isSymbol, setisSymbol] = useState(false);

  useEffect(() => {
    localStorage.setItem("isLoginPointeuse", false);
  }, []);

  const handleChangeCode = (value) => {
    if (value.length > 7) {
      setisLength(false);
    } else {
      setisLength(true);
    }

    if (hasUperCase(value)) {
      setisUpercase(false);
    } else {
      setisUpercase(true);
    }

    if (hasNumber(value)) {
      setisNumber(false);
    } else {
      setisNumber(true);
    }

    if (hasSymbole(value)) {
      setisSymbol(true);
    } else {
      setisSymbol(false);
    }

    setcode(value);
  };

  const handleSaveConfig = () => {
    if (
      code === null ||
      nbHoureWork === null ||
      startTime === null ||
      endTime === null ||
      nbJourWorkWeek === null ||
      breakTime === null ||
      totalConge === null ||
      retardMonth === null ||
      retardDay === null
    ) {
      //toast.error("vérifié les champs vide");
      alert.error("vérifié les champs vides");
    } else {
      postSettings(
        code,
        nbHoureWork,
        startTime,
        endTime,
        nbJourWorkWeek,
        breakTime,
        totalConge,
        retardMonth,
        retardDay
      ).then(() => {
        addCode(code).then(() => {
          alert.success("configuration ajouté");
          history.push("/");
        });
      });
    }
  };

  return (
    <div className="page login-bg mt-4">
      <div className="page-single">
        <div className="container">
          <div className="row ">
            <div className="col-md-12">
              <div className="card">
                <div className="card-header border-bottom-0">
                  <div className="card-title">Configuration de session administrateur</div>
                </div>
                <div className="card-body">
                  <div id="wizard1">
                    <h3>Code société</h3>
                    <section>
                      <div className="control-group form-group">
                        <label className="form-label">
                          Code<span className="text-danger"> *</span>
                        </label>

                        <input
                          type="text"
                          className="form-control required"
                          placeholder="code"
                          required
                          value={code}
                          onChange={(e) => handleChangeCode(e.target.value)}
                        />
                        <small className="form-text text-muted  mb-5">
                          Ce code est obligatoire il sera utilisé ensuite pour ajouter des collaborateurs.
                        </small>

                        {isLength && (
                          <div className="alert alert-warning" role="alert">
                            Le code doit contenir au mois 8 caractères
                          </div>
                        )}
                        {isUpercase && (
                          <div className="alert alert-warning" role="alert">
                            Le code doit contenir majuscule
                          </div>
                        )}
                        {isNumber && (
                          <div className="alert alert-warning" role="alert">
                            Le code doit contenir chiffre
                          </div>
                        )}

                        {isSymbol && (
                          <div className="alert alert-danger" role="alert">
                            Pas de symboles
                          </div>
                        )}
                      </div>
                    </section>

                    <h3>Paramètres générales</h3>
                    <section>
                      <div className="row">
                        <div className="col-4">
                          <div className="form-group">
                            <label className="form-label">
                              Nombre des heures de travail par jour<span className="text-danger"> *</span>
                            </label>
                            <input
                              type="number"
                              className="form-control"
                              id="name1"
                              placeholder="ex : 1"
                              required
                              value={nbHoureWork}
                              onChange={(e) => setnbHoureWork(e.target.value)}
                            />
                          </div>
                        </div>
                        <div className="col-4">
                          <div className="form-group">
                            <label className="form-label">
                              Nombre des jours de travail par semaine<span className="text-danger"> *</span>
                            </label>
                            <input
                              type="number"
                              className="form-control"
                              id="name1"
                              placeholder="ex : 1"
                              required
                              value={nbJourWorkWeek}
                              onChange={(e) => setnbJourWorkWeek(e.target.value)}
                            />
                          </div>
                        </div>
                        <div className="col-4">
                          <div className="form-group">
                            <label className="form-label">
                              Total Congé<span className="text-danger"> *</span>
                            </label>
                            <input
                              type="number"
                              className="form-control"
                              id="name1"
                              placeholder="ex : 1"
                              required
                              value={totalConge}
                              onChange={(e) => settotalConge(e.target.value)}
                            />
                          </div>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-4">
                          <div className="form-group">
                            <label className="form-label">
                              Heure début<span className="text-danger"> *</span>
                            </label>
                            <input
                              type="time"
                              className="form-control"
                              id="name1"
                              placeholder="First Name"
                              required
                              value={startTime}
                              onChange={(e) => setstartTime(e.target.value)}
                            />
                          </div>
                        </div>
                        <div className="col-4">
                          <div className="form-group">
                            <label className="form-label">
                              Heure fin<span className="text-danger"> *</span>
                            </label>
                            <input
                              type="time"
                              className="form-control"
                              id="name1"
                              placeholder="First Name"
                              required
                              value={endTime}
                              onChange={(e) => setendTime(e.target.value)}
                            />
                          </div>
                        </div>
                        <div className="col-4">
                          <div className="form-group">
                            <label className="form-label">
                              Durée de pause<span className="text-danger"> *</span>
                            </label>
                            <input
                              type="time"
                              className="form-control"
                              id="name1"
                              placeholder="First Name"
                              required
                              value={breakTime}
                              onChange={(e) => setbreakTime(e.target.value)}
                            />
                            <small className="form-text text-muted  mb-5">note : pour une heure choisir 01:00</small>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-6">
                          <div className="form-group">
                            <label className="form-label">
                              Retard par jour<span className="text-danger"> *</span>
                            </label>
                            <input
                              type="time"
                              className="form-control"
                              id="name1"
                              placeholder="First Name"
                              required
                              value={retardDay}
                              onChange={(e) => setretardDay(e.target.value)}
                            />
                            <small className="form-text text-muted  mb-5">
                              note : pour une demi-heure choisir 00:30
                            </small>
                          </div>
                        </div>
                        <div className="col-6">
                          <div className="form-group">
                            <label className="form-label">
                              Retard par mois<span className="text-danger"> *</span>
                            </label>
                            <input
                              type="time"
                              className="form-control"
                              id="name1"
                              placeholder="First Name"
                              required
                              value={retardMonth}
                              onChange={(e) => setretardMonth(e.target.value)}
                            />
                            <small className="form-text text-muted  mb-5">note : pour 3 heures choisir 03:00</small>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>

                {hasNumber(code) && hasUperCase(code) && code.length > 7 && !hasSymbole(code) && (
                  <div className="card-footer text-right">
                    <Link to="#" className="btn btn-primary btn-lg" onClick={handleSaveConfig}>
                      Enregistrer
                    </Link>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default WizardConfig;
