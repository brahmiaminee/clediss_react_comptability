import React from "react";

function Test() {
  return (
    <div>
      <div className="page-header d-lg-flex d-block">
        <div className="page-leftheader">
          <h4 className="page-title">Carousedl</h4>
        </div>
        <div className="page-rightheader ml-md-auto">
          <div className=" btn-list">
            <button className="btn btn-light" data-toggle="tooltip" data-placement="top" title="E-mail">
              {" "}
              <i className="feather feather-mail" />{" "}
            </button>
            <button className="btn btn-light" data-placement="top" data-toggle="tooltip" title="Contact">
              {" "}
              <i className="feather feather-phone-call" />{" "}
            </button>
            <button className="btn btn-primary" data-placement="top" data-toggle="tooltip" title="Info">
              {" "}
              <i className="feather feather-info" />{" "}
            </button>
          </div>
        </div>
      </div>
      {/*End Page header*/}
      {/* Row */}
      <div className="row">
        <div className="col-md-12 col-lg-4">
          <div className="card">
            <div className="card-header border-bottom-0">
              <h3 className="card-title">Carousel with controls</h3>
            </div>
            <div className="card-body">
              <div id="carousel-controls" className="carousel slide" data-ride="carousel">
                <div className="carousel-inner">
                  <div className="carousel-item active">
                    <img className="d-block w-100" alt src="assets/images/photos/4.jpg" data-holder-rendered="true" />
                  </div>
                  <div className="carousel-item">
                    <img className="d-block w-100" alt src="assets/images/photos/5.jpg" data-holder-rendered="true" />
                  </div>
                  <div className="carousel-item">
                    <img className="d-block w-100" alt src="assets/images/photos/6.jpg" data-holder-rendered="true" />
                  </div>
                  <div className="carousel-item">
                    <img className="d-block w-100" alt src="assets/images/photos/7.jpg" data-holder-rendered="true" />
                  </div>
                  <div className="carousel-item">
                    <img className="d-block w-100" alt src="assets/images/photos/8.jpg" data-holder-rendered="true" />
                  </div>
                </div>
                <a className="carousel-control-prev" href="#carousel-controls" role="button" data-slide="prev">
                  <span className="carousel-control-prev-icon" aria-hidden="true" />
                  <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carousel-controls" role="button" data-slide="next">
                  <span className="carousel-control-next-icon" aria-hidden="true" />
                  <span className="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Test;
