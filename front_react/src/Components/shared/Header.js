import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { getUserById } from "../../Services/Pointeuse/UsersApi";
import { useTranslation } from "react-i18next";

function Header() {
  const { t, i18n } = useTranslation();
  const history = useHistory();
  const [data, setdata] = useState({});
  const [isOpen, setIsOpen] = useState(false);
  const [searchText, setsearchText] = useState(null);

  useEffect(() => {
    // document.body.classList.add("sidebar-collapse");
    getUserById(localStorage.getItem("id")).then((res) => {
      setdata(res.data);
    });
  }, []);

  const handleClick = () => {
    if (isOpen) {
      document.body.classList.remove("dark-mode");
      localStorage.setItem("pointeuse_dark", false);
      setIsOpen(!isOpen);
    } else {
      document.body.classList.add("dark-mode");
      localStorage.setItem("pointeuse_dark", true);
      setIsOpen(!isOpen);
    }
  };
  const handleLogout = () => {
    localStorage.setItem("isLoginPointeuse", false);
    history.push("/login");
  };

  const handleInstall = () => {
    let deferredPrompt;

    window.addEventListener("beforeinstallprompt", (e) => {
      console.log(e);
      const addBtn = document.querySelector(".zz");
      // Prevent Chrome 67 and earlier from automatically showing the prompt
      e.preventDefault();
      // Stash the event so it can be triggered later.
      deferredPrompt = e;
      // Update UI to notify the user they can add to home screen
      addBtn.style.display = "block";

      addBtn.addEventListener("click", () => {
        // hide our user interface that shows our A2HS button
        addBtn.style.display = "none";
        // Show the prompt
        deferredPrompt.prompt();
        // Wait for the user to respond to the prompt
        deferredPrompt.userChoice.then((choiceResult) => {
          if (choiceResult.outcome === "accepted") {
            console.log("User accepted the A2HS prompt");
          } else {
            console.log("User dismissed the A2HS prompt");
          }
          deferredPrompt = null;
        });
      });
    });
  };

  const handleChangeL = (l) => {
    console.log(l);
    i18n.changeLanguage(l);
  };

  const handleGoToSearch = (e) => {
    e.preventDefault();
    history.push({ pathname: "/admin/search", state: searchText });
    setsearchText("");
  };
  return (
    <div className="app-header header">
      <div className="container-fluid">
        <div className="d-flex">
          <a className="header-brand" href="index.html">
            <img src="assets/images/brand/logo.png" className="header-brand-img desktop-lgo" alt="Dayonelogo" />
            <img src="assets/images/brand/logo-white.png" className="header-brand-img dark-logo" alt="Dayonelogo" />
            <img src="assets/images/brand/favicon.png" className="header-brand-img mobile-logo" alt="Dayonelogo" />
            <img src="assets/images/brand/favicon1.png" className="header-brand-img darkmobile-logo" alt="Dayonelogo" />
          </a>
          <div className="app-sidebar__toggle" data-toggle="sidebar">
            <a className="open-toggle" href="#">
              <i className="feather feather-menu" />
            </a>
            <a className="close-toggle" href="#">
              <i className="feather feather-x" />
            </a>
          </div>
          <div className="mt-0">
            <form className="form-inline" onSubmit={handleGoToSearch}>
              <div className="search-element">
                <input
                  type="search"
                  className="form-control header-search"
                  placeholder="Recherche ..."
                  aria-label="Search"
                  value={searchText}
                  onChange={(e) => setsearchText(e.target.value)}
                  tabIndex={1}
                />
                <button className="btn btn-primary-color" type="submit">
                  <i className="feather feather-search" />
                </button>
              </div>
            </form>
          </div>
          {/* SEARCH */}
          <div className="d-flex order-lg-2 my-auto ml-auto">
            <a className="nav-link my-auto icon p-0 nav-link-lg d-md-none navsearch" href="#" data-toggle="search">
              <i className="feather feather-search search-icon header-icon" />
            </a>
            <div className="dropdown header-flags">
              <a className="nav-link icon" data-toggle="dropdown">
                <img src="assets/images/flags/flag-png/france.png" className="h-24" alt="img" />
              </a>
              <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow animated">
                <Link to="#" className="dropdown-item d-flex " onClick={() => handleChangeL("ar")}>
                  {" "}
                  <span className="avatar  mr-3 align-self-center bg-transparent">
                    <img src="assets/images/flags/flag-png/tunisia.png" alt="img" className="h-24" />
                  </span>
                  <div className="d-flex">
                    {" "}
                    <span className="my-auto">{t("arabe")}</span>{" "}
                  </div>
                </Link>
                <Link to="#" className="dropdown-item d-flex " onClick={() => handleChangeL("fr")}>
                  {" "}
                  <span className="avatar  mr-3 align-self-center bg-transparent">
                    <img src="assets/images/flags/flag-png/france.png" alt="img" className="h-24" />
                  </span>
                  <div className="d-flex">
                    {" "}
                    <span className="my-auto">{t("francais")}</span>{" "}
                  </div>
                </Link>
                <Link to="#" className="dropdown-item d-flex " onClick={() => handleChangeL("en")}>
                  {" "}
                  <span className="avatar  mr-3 align-self-center bg-transparent">
                    <img src="assets/images/flags/flag-png/united-kingdom.png" alt="img" className="h-24" />
                  </span>
                  <div className="d-flex">
                    {" "}
                    <span className="my-auto">{t("englais")}</span>{" "}
                  </div>
                </Link>
                {/* <Link to ="#" className="dropdown-item d-flex">
                  {" "}
                  <span className="avatar  mr-3 align-self-center bg-transparent">
                    <img src="assets/images/flags/flag-png/united-kingdom.png" alt="img" className="h-24" />
                  </span>
                  <div className="d-flex">
                    {" "}
                    <span className="my-auto">UK</span>{" "}
                  </div>
                </Link>
                <Link to ="#" className="dropdown-item d-flex">
                  {" "}
                  <span className="avatar mr-3 align-self-center bg-transparent">
                    <img src="assets/images/flags/flag-png/italy.png" alt="img" className="h-24" />
                  </span>
                  <div className="d-flex">
                    {" "}
                    <span className="my-auto">Italy</span>{" "}
                  </div>
                </Link>
                <Link to ="#" className="dropdown-item d-flex">
                  {" "}
                  <span className="avatar mr-3 align-self-center bg-transparent">
                    <img src="assets/images/flags/flag-png/united-states-of-america.png" className="h-24" alt="img" />
                  </span>
                  <div className="d-flex">
                    {" "}
                    <span className="my-auto">US</span>{" "}
                  </div>
                </Link>
                <Link to ="#" className="dropdown-item d-flex">
                  {" "}
                  <span className="avatar  mr-3 align-self-center bg-transparent">
                    <img src="assets/images/flags/flag-png/spain.png" alt="img" className="h-24" />
                  </span>
                  <div className="d-flex">
                    {" "}
                    <span className="my-auto">Spain</span>{" "}
                  </div>
                </Link> */}
              </div>
            </div>
            <div className="dropdown header-fullscreen">
              <a className="nav-link icon full-screen-link">
                <i className="feather feather-maximize fullscreen-button fullscreen header-icons" />
                <i className="feather feather-minimize fullscreen-button exit-fullscreen header-icons" />
              </a>
            </div>

            <div className="dropdown header-fullscreen" onClick={handleClick}>
              <a className="nav-link icon full-screen-link">
                <i className="feather feather-sun header-icon" />
              </a>
            </div>
            <div className="dropdown header-fullscreen zz" onClick={handleInstall}>
              <a className="nav-link icon full-screen-link">
                <i className="feather feather-download header-icon" />
              </a>
            </div>
            <div className="dropdown header-message">
              <a className="nav-link icon" data-toggle="dropdown">
                <i className="feather feather-mail header-icon" />
                <span className="badge badge-success side-badge">5</span>
              </a>
              <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow  animated">
                <div className="header-dropdown-list message-menu" id="message-menu">
                  <a className="dropdown-item border-bottom" href="#">
                    <div className="d-flex align-items-center">
                      <div>
                        <span
                          className="avatar avatar-md brround align-self-center cover-image"
                          data-image-src="assets/images/users/1.jpg"
                        />
                      </div>
                      <div className="d-flex">
                        <div className="pl-3">
                          <h6 className="mb-1">Jack Wright</h6>
                          <p className="fs-13 mb-1">All the best your template awesome</p>
                          <div className="small text-muted">3 hours ago</div>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a className="dropdown-item border-bottom" href="#">
                    <div className="d-flex align-items-center">
                      <div>
                        <span
                          className="avatar avatar-md brround align-self-center cover-image"
                          data-image-src="assets/images/users/2.jpg"
                        />
                      </div>
                      <div className="d-flex">
                        <div className="pl-3">
                          <h6 className="mb-1">Lisa Rutherford</h6>
                          <p className="fs-13 mb-1">Hey! there I'm available</p>
                          <div className="small text-muted">5 hour ago</div>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a className="dropdown-item border-bottom" href="#">
                    <div className="d-flex align-items-center">
                      <div>
                        <span
                          className="avatar avatar-md brround align-self-center cover-image"
                          data-image-src="assets/images/users/3.jpg"
                        />
                      </div>
                      <div className="d-flex">
                        <div className="pl-3">
                          <h6 className="mb-1">Blake Walker</h6>
                          <p className="fs-13 mb-1">Just created a new blog post</p>
                          <div className="small text-muted">45 mintues ago</div>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a className="dropdown-item border-bottom" href="#">
                    <div className="d-flex align-items-center">
                      <div>
                        <span
                          className="avatar avatar-md brround align-self-center cover-image"
                          data-image-src="assets/images/users/4.jpg"
                        />
                      </div>
                      <div className="d-flex">
                        <div className="pl-3">
                          <h6 className="mb-1">Fiona Morrison</h6>
                          <p className="fs-13 mb-1">Added new comment on your photo</p>
                          <div className="small text-muted">2 days ago</div>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a className="dropdown-item border-bottom" href="#">
                    <div className="d-flex align-items-center">
                      <div>
                        <span
                          className="avatar avatar-md brround align-self-center cover-image"
                          data-image-src="assets/images/users/6.jpg"
                        />
                      </div>
                      <div className="d-flex">
                        <div className="pl-3">
                          <h6 className="mb-1">Stewart Bond</h6>
                          <p className="fs-13 mb-1">Your payment invoice is generated</p>
                          <div className="small text-muted">3 days ago</div>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
                <div className=" text-center p-2">
                  <Link to="#">See All Messages</Link>
                </div>
              </div>
            </div>
            <div className="dropdown header-notify">
              <a className="nav-link icon" data-toggle="sidebar-right" data-target=".sidebar-right">
                <i className="feather feather-bell header-icon" />
                <span className="bg-dot" />
              </a>
            </div>
            <div className="dropdown profile-dropdown">
              <Link to="#" className="nav-link pr-1 pl-0 leading-none" data-toggle="dropdown">
                <span>
                  <img
                    src={
                      data.img == null || data.img == ""
                        ? "assets/images/users/avatar.png"
                        : localStorage.getItem("baseUrl") + data.img
                    }
                    onError={(e) => {
                      e.target.onerror = null;
                      e.target.src = "assets/images/users/avatar.png";
                    }}
                    alt="img"
                    className="avatar avatar-md bradius"
                  />
                </span>
              </Link>
              <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow animated">
                <div className="p-3 text-center border-bottom">
                  <Link to="#" className="text-center user pb-0 font-weight-bold">
                    {data.nom} {data.prenom}
                  </Link>
                  <p className="text-center user-semi-title">{data.fonction}</p>
                </div>
                <a className="dropdown-item d-flex" href="#">
                  <i className="feather feather-user mr-3 fs-16 my-auto" />
                  <div className="mt-1">Profile</div>
                </a>
                <a className="dropdown-item d-flex" href="#">
                  <i className="feather feather-settings mr-3 fs-16 my-auto" />
                  <div className="mt-1">Settings</div>
                </a>
                {/* <a className="dropdown-item d-flex" href="#">
                  <i className="feather feather-mail mr-3 fs-16 my-auto" />
                  <div className="mt-1">Messages</div>
                </a>
                <a className="dropdown-item d-flex" href="#" data-toggle="modal" data-target="#changepasswordnmodal">
                  <i className="feather feather-edit-2 mr-3 fs-16 my-auto" />
                  <div className="mt-1">Change Password</div>
                </a> */}
                <a className="dropdown-item d-flex" href="#">
                  <i className="feather feather-power mr-3 fs-16 my-auto" />
                  <div className="mt-1" onClick={handleLogout}>
                    Sign Out
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Header;
