import React from "react";

function Preloader() {
  return (
    <div id="global-loader">
      <img src="assets/images/svgs/loader.svg" alt="loader" />
    </div>
  );
}

export default Preloader;
