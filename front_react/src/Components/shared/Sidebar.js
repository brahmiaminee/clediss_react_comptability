import $ from "jquery";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { getUserById } from "../../Services/Pointeuse/UsersApi";

function Sidebar() {
  const [data, setdata] = useState({});
  useEffect(() => {
    // Toggle Sidebar
    $(document).on("click", '[data-toggle="sidebar"]', function (event) {
      event.preventDefault();
      $(".app").toggleClass("sidenav-toggled");
    });

    $(".app-sidebar").hover(
      function () {
        if ($(".app").hasClass("sidenav-toggled")) {
          $(".app").addClass("sidenav-toggled1");
        }
      },
      function () {
        if ($(".app").hasClass("sidenav-toggled")) {
          $(".app").removeClass("sidenav-toggled1");
        }
      }
    );

    // Activate sidebar slide toggle
    $("[data-toggle='slide']").on("click", function (e) {
      var $this = $(this);
      var checkElement = $this.next();
      var animationSpeed = 300,
        slideMenuSelector = ".slide-menu";
      if (checkElement.is(slideMenuSelector) && checkElement.is(":visible")) {
        checkElement.slideUp(animationSpeed, function () {
          checkElement.removeClass("open");
        });
        checkElement.parent("li").removeClass("is-expanded");
      } else if (checkElement.is(slideMenuSelector) && !checkElement.is(":visible")) {
        var parent = $this.parents("ul").first();
        var ul = parent.find("ul:visible").slideUp(animationSpeed);
        ul.removeClass("open");
        var parent_li = $this.parent("li");
        checkElement.slideDown(animationSpeed, function () {
          checkElement.addClass("open");
          parent.find("li.is-expanded").removeClass("is-expanded");
          parent_li.addClass("is-expanded");
        });
      }
      if (checkElement.is(slideMenuSelector)) {
        e.preventDefault();
      }
    });

    // Activate sidebar slide toggle
    $("[data-toggle='sub-slide']").on("click", function (e) {
      var $this = $(this);
      var checkElement = $this.next();
      var animationSpeed = 300,
        slideMenuSelector = ".sub-slide-menu";
      if (checkElement.is(slideMenuSelector) && checkElement.is(":visible")) {
        checkElement.slideUp(animationSpeed, function () {
          checkElement.removeClass("open");
        });
        checkElement.parent("li").removeClass("is-expanded");
      } else if (checkElement.is(slideMenuSelector) && !checkElement.is(":visible")) {
        var parent = $this.parents("ul").first();
        var ul = parent.find("ul:visible").slideUp(animationSpeed);
        ul.removeClass("open");
        var parent_li = $this.parent("li");
        checkElement.slideDown(animationSpeed, function () {
          checkElement.addClass("open");
          parent.find("li.is-expanded").removeClass("is-expanded");
          parent_li.addClass("is-expanded");
        });
      }
      if (checkElement.is(slideMenuSelector)) {
        e.preventDefault();
      }
    });

    // Activate sidebar slide toggle
    $("[data-toggle='sub-slide2']").on("click", function (e) {
      var $this = $(this);
      var checkElement = $this.next();
      var animationSpeed = 300,
        slideMenuSelector = ".sub-slide-menu2";
      if (checkElement.is(slideMenuSelector) && checkElement.is(":visible")) {
        checkElement.slideUp(animationSpeed, function () {
          checkElement.removeClass("open");
        });
        checkElement.parent("li").removeClass("is-expanded");
      } else if (checkElement.is(slideMenuSelector) && !checkElement.is(":visible")) {
        var parent = $this.parents("ul").first();
        var ul = parent.find("ul:visible").slideUp(animationSpeed);
        ul.removeClass("open");
        var parent_li = $this.parent("li");
        checkElement.slideDown(animationSpeed, function () {
          checkElement.addClass("open");
          parent.find("li.is-expanded").removeClass("is-expanded");
          parent_li.addClass("is-expanded");
        });
      }
      if (checkElement.is(slideMenuSelector)) {
        e.preventDefault();
      }
    });

    //Activate bootstrip tooltips

    // ______________Active Class
    $(document).ready(function () {
      $(".app-sidebar li a").each(function () {
        var pageUrl = window.location.href.split(/[?#]/)[0];
        if (this.href == pageUrl) {
          $(this).addClass("active");
          $(this).parent().addClass("is-expanded");
          $(this).parent().parent().prev().addClass("active");
          $(this).parent().parent().addClass("open");
          $(this).parent().parent().prev().addClass("is-expanded");
          $(this).parent().parent().parent().addClass("is-expanded");
          $(this).parent().parent().parent().parent().addClass("open");
          $(this).parent().parent().parent().parent().prev().addClass("active");
          $(this).parent().parent().parent().parent().parent().addClass("is-expanded");
          $(this).parent().parent().parent().parent().parent().parent().prev().addClass("active");
          $(this).parent().parent().parent().parent().parent().parent().parent().addClass("is-expanded");
        }
      });
    });
  }, []);
  useEffect(() => {
    // document.body.classList.add("sidebar-collapse");
    getUserById(localStorage.getItem("id")).then((res) => {
      setdata(res.data);
    });
  }, []);
  return (
    <aside className="app-sidebar">
      <div className="app-sidebar__logo">
        <Link to="/" className="header-brand">
          <img src="assets/images/brand/logo.png" className="header-brand-img desktop-lgo" alt="Dayonelogo" />
          <img src="assets/images/brand/logo-white.png" className="header-brand-img dark-logo" alt="Dayonelogo" />
          <img src="assets/images/brand/logo.png" className="header-brand-img mobile-logo" alt="Dayonelogo" />
          <img src="assets/images/brand/favicon1.png" className="header-brand-img darkmobile-logo" alt="Dayonelogo" />
        </Link>
      </div>
      <div className="app-sidebar3">
        <div className="app-sidebar__user">
          <div className="dropdown user-pro-body text-center">
            <div className="user-pic">
              <img
                src={
                  data.img == null || data.img == ""
                    ? "assets/images/users/avatar.png"
                    : localStorage.getItem("baseUrl") + data.img
                }
                onError={(e) => {
                  e.target.onerror = null;
                  e.target.src = "assets/images/users/avatar.png";
                }}
                alt="user-img"
                className="avatar-xxl rounded-circle mb-1"
              />
            </div>
            <div className="user-info">
              <h5 className=" mb-2">
                {data.nom} {data.prenom}
              </h5>
              <span className="text-muted app-sidebar__user-name text-sm">{data.fonction}</span>
            </div>
          </div>
        </div>
        <ul className="side-menu">
          <li className="side-item side-item-category mt-4">Dashboards</li>
          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="feather feather-users sidemenu_icon" />
              <span className="side-menu__label">Collaborateur</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/users" className="slide-item">
                  List collaborateurs
                </Link>
              </li>
              <li>
                <Link to="/admin/global" className="slide-item">
                  Statistique Collaborateurs
                </Link>
              </li>
              <li>
                <Link to="/admin/retard" className="slide-item">
                  List Retards
                </Link>
              </li>
              <li>
                <Link to="/admin/questionnaire" className="slide-item">
                  List questionnaires
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              {/* <i className="feather feather-home sidemenu_icon" /> */}
              <i className="fa fa-shield sidemenu_icon"></i>

              <span className="side-menu__label">Covid et vaccination</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/evaxinfo" className="slide-item">
                  covid-19
                </Link>
              </li>
              <li>
                <Link to="/admin/vaccin" className="slide-item">
                  List vaccinés
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-calendar sidemenu_icon" />
              <span className="side-menu__label">Congés</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/holiday" className="slide-item">
                  Jours fériés
                </Link>
              </li>
            </ul>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/calendarconge" className="slide-item">
                  Calendrier
                </Link>
              </li>
            </ul>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/demandeconge" className="slide-item">
                  Demande Congés
                </Link>
              </li>
            </ul>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/congevalide" className="slide-item">
                  Congés validés
                </Link>
              </li>
            </ul>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/congenonvalide" className="slide-item">
                  Congés non validés
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-check sidemenu_icon" />
              <span className="side-menu__label">Pointage</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/jourpointage" className="slide-item">
                  Pointage de jour
                </Link>
              </li>
              <li>
                <Link to="/admin/moispointage" className="slide-item">
                  Pointage du mois
                </Link>
              </li>
              <li>
                <Link to="/admin/pointagemanquant" className="slide-item">
                  Pointage manquant
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-home sidemenu_icon" />
              <span className="side-menu__label">Télétravail</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/teletravail" className="slide-item">
                  Demande Télétravail
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="feather feather-briefcase sidemenu_icon" />
              <span className="side-menu__label">Projets</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/projects" className="slide-item">
                  List projets
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="feather feather-clipboard sidemenu_icon" />
              <span className="side-menu__label">Tâche</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/task" className="slide-item">
                  List Tâche
                </Link>
              </li>
              <li>
                <Link to="/admin/kanban" className="slide-item">
                  Kanban
                </Link>
              </li>
              <li>
                <Link to="/admin/gantt" className="slide-item">
                  Tableau des tâches
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-align-center sidemenu_icon" />
              <span className="side-menu__label">TODO</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/todo" className="slide-item">
                  List TODO
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-pie-chart sidemenu_icon" />
              <span className="side-menu__label">Sondage</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/sondage" className="slide-item">
                  List Sondage
                </Link>
              </li>
            </ul>
          </li>
          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-wpforms sidemenu_icon" />
              <span className="side-menu__label">Node de frais</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/notefrais" className="slide-item">
                  List note de frais
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-automobile sidemenu_icon" />
              <span className="side-menu__label">Déplacement</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/deplacement" className="slide-item">
                  List déplacement
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-arrow-circle-o-right sidemenu_icon" />
              <span className="side-menu__label">Autorisation</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/autorisation" className="slide-item">
                  List Autorisation
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-flag sidemenu_icon" />
              <span className="side-menu__label">Note de service</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/annonce" className="slide-item">
                  List Annonces
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-money sidemenu_icon" />
              <span className="side-menu__label">Acompte sur salaire</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/acomptesalaire" className="slide-item">
                  List Acompte sur salaire
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-sitemap sidemenu_icon" />
              <span className="side-menu__label">Départements</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/departements" className="slide-item">
                  List Départements
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-desktop sidemenu_icon" />
              <span className="side-menu__label">Demande matériel</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/materiel" className="slide-item">
                  List Demande matériel
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-gears sidemenu_icon" />
              <span className="side-menu__label">Demande maintenance</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/maintenance" className="slide-item">
                  List Demande maintenance
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-laptop sidemenu_icon" />
              <span className="side-menu__label">Formations</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/formations" className="slide-item">
                  List Formation
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-hospital-o sidemenu_icon" />
              <span className="side-menu__label">Entreprise</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/entreprise" className="slide-item">
                  Paramètre entreprise
                </Link>
              </li>
              <li>
                <Link to="/admin/baremes" className="slide-item">
                  Barème
                </Link>
              </li>
              {/* <li>
                <Link to="/admin/lignepaie" className="slide-item">
                  Ligne de paie
                </Link>
              </li> */}
              <li>
                <Link to="/admin/salaire" className="slide-item">
                  Virement salaire
                </Link>
              </li>
              <li>
                <Link to="/admin/cheque" className="slide-item">
                  Chèque tiroir
                </Link>
              </li>
              {/* <li>
                <Link to="/admin/paramfacture" className="slide-item">
                  Paramètre facture
                </Link>
              </li> */}
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="feather feather-headphones sidemenu_icon" />
              <span className="side-menu__label">Support</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/support" className="slide-item">
                  support
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-newspaper-o sidemenu_icon" />
              <span className="side-menu__label">Blog</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/blog" className="slide-item">
                  Blog
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-clone sidemenu_icon" />
              <span className="side-menu__label">Document</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/fileconge" className="slide-item">
                  Document
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-line-chart sidemenu_icon" />
              <span className="side-menu__label">Objectif</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/objectif" className="slide-item">
                  Objectif
                </Link>
              </li>
            </ul>
          </li>

          <li className="side-item side-item-category">Facturation</li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-cube sidemenu_icon" />
              <span className="side-menu__label">Produits</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/produit" className="slide-item">
                  List Produits
                </Link>
              </li>
              <li>
                <Link to="/admin/facturation/productparam" className="slide-item">
                  Paramétrage produit
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="feather feather-users sidemenu_icon" />
              <span className="side-menu__label">Client</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/client" className="slide-item">
                  List clients
                </Link>
              </li>
            </ul>
          </li>
          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="feather feather-users sidemenu_icon" />
              <span className="side-menu__label">Fournisseur</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/fournisseur" className="slide-item">
                  List fournisseurs
                </Link>
              </li>
            </ul>
          </li>
          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-money sidemenu_icon" />
              <span className="side-menu__label">Tax</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/tax" className="slide-item">
                  List Tax
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-list-alt sidemenu_icon" />
              <span className="side-menu__label">Préfix</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/prefix" className="slide-item">
                  List préfix
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-usd sidemenu_icon" />
              <span className="side-menu__label">Encaissement</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/encaissement" className="slide-item">
                  List encaissement
                </Link>
              </li>
            </ul>
          </li>
          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-usd sidemenu_icon" />
              <span className="side-menu__label">Paiement</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/paiement" className="slide-item">
                  List paiement
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-copy sidemenu_icon" />
              <span className="side-menu__label">Document de vente</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/facturevente" className="slide-item">
                  Facture
                </Link>
              </li>
            </ul>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/blvente" className="slide-item">
                  Bon de livraison
                </Link>
              </li>
            </ul>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/devisvente" className="slide-item">
                  Devis
                </Link>
              </li>
            </ul>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/commandevente" className="slide-item">
                  Commande
                </Link>
              </li>
            </ul>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/listabonnement" className="slide-item">
                  abonnement
                </Link>
              </li>
            </ul>
          </li>

          <li className="slide">
            <a className="side-menu__item" data-toggle="slide" href="#">
              <i className="fa fa-copy sidemenu_icon" />
              <span className="side-menu__label">Document d'achat</span>
              <i className="angle fa fa-angle-right" />
            </a>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/factureachat" className="slide-item">
                  Facture
                </Link>
              </li>
            </ul>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/blachat" className="slide-item">
                  Bon de livraison
                </Link>
              </li>
            </ul>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/devisachat" className="slide-item">
                  Devis
                </Link>
              </li>
            </ul>
            <ul className="slide-menu">
              <li>
                <Link to="/admin/commandeachat" className="slide-item">
                  commande
                </Link>
              </li>
            </ul>
          </li>
        </ul>

        {/* khkjklj */}
        <div className="Annoucement_card">
          <div className="text-center">
            <div>
              <h5 className="title mt-0 mb-1 ml-2 font-weight-bold tx-12">Note de service</h5>
              <div className="bg-layer py-4">
                <img src="assets/images/photos/announcement-1.png" className="py-3 text-center mx-auto" alt="img" />
              </div>
              <p className="subtext mt-0 mb-0 ml-2 fs-13 text-center my-2">
                Faire une annonce pour votre collaborateurs
              </p>
            </div>
          </div>
          <Link to="/admin/annonce">
            <button className="btn btn-block btn-primary my-4 fs-12">Ajouter annonce</button>
          </Link>
          <Link to="/admin/annonce">
            <button className="btn btn-block btn-outline fs-12">voir historique</button>
          </Link>
        </div>
      </div>
    </aside>
  );
}

export default Sidebar;
