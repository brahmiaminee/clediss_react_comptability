import React from "react";

function Footer() {
  return (
    <footer className="footer">
      <div className="container">
        <div className="row align-items-center flex-row-reverse">
          <div className="col-md-12 col-sm-12 mt-3 mt-lg-0 text-center">
            Copyright © 2022 <a href="http://www.nomadis.online">Clediss</a>.<a href="#"></a> All rights reserved.
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
