/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";
import { SRLWrapper } from "simple-react-lightbox";

export class ImageComponent extends Component {
  state = { isOpen: false };

  handleShowDialog = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    return (
      <SRLWrapper>
        <img
          style={{ minWidth: 25 }}
          className={this.props.atr ? this.props.atr : "avatar avatar-md brround mr-3"}
          src={
            this.props.picture ? localStorage.getItem("baseUrl") + this.props.picture : "assets/images/users/avatar.png"
          }
          onError={(e) => {
            e.target.onerror = null;
            e.target.src = "assets/images/users/avatar.png";
          }}
          loading="lazy"
          onClick={this.handleShowDialog}
          alt="no image"
        />
      </SRLWrapper>
    );
  }
}

export default ImageComponent;
