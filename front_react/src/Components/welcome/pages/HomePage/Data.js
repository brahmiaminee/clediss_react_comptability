export const homeObjOne = {
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: "Poiteuse",
  headline: "Commencer à enregistrer votre temps de travail.",
  description:
    "Pointeuse est une solution moderme pour la gestion du temps de travail, tâches et congés des collaborateurs en ligne.",
  buttonLabel: "Commencer",
  imgStart: "",
  img: "assets/images/welcome/svg-1.svg",
  alt: "Credit Card",
};

export const homeObjTwo = {
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: "POURQOUI POINTEUSE ?",
  headline: "Pointage basé sur la reconnaissance faciale",
  description:
    "L’application Checkin doit être capable d’apprendre des visages des collaborateurs par le biais d’une caméra d'un smartphone et de les stocker dans sa base pour pouvoir les reconnaître à tout moment.À la reconnaissance d’un visage d’un employé elle doit tout de suite l’associer à son identitée personnelle et professionnelle. Elle doit aussi comptabiliser l’heure de son passage à l’occasion d’une entrée ou d’une sortie",
  buttonLabel: "Learn More",
  imgStart: "",
  img: "assets/images/welcome/svg-5.svg",
  alt: "Vault",
};

export const homeObjThree = {
  lightBg: true,
  lightText: false,
  lightTextDesc: false,
  topLine: "À PROPOS CHECKIN",
  headline: "Faites pointer vos employés à partir de leur propre téléphone intelligent. ",
  description:
    "Parfaite pour les employés mobiles, l’application envoie automatiquement les heures d’entrée, de pause et de sortie à vos feuilles de temps. Plus précis, plus justeLa manière de suivre les heures de travail a évolué. Allégez votre fardeau de gestion grâce à un système de pointage moderne qui s’adapte parfaitement à votre milieu de travail",
  buttonLabel: "Start Now",
  imgStart: "start",
  img: "assets/images/welcome/svg-7.svg",
  alt: "Vault",
};

export const homeObjFour = {
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: "Téléchargez l'appli",
  headline: "Google play",
  description:
    "L'application la plus simple, Cliquez sur un bouton pour pointer en entrée et commencer à suivre les heures effectuées.",
  buttonLabel: "Télecharger",
  imgStart: "start",
  img: "assets/images/welcome/svg-2.svg",
  alt: "Vault",
};
