import React from "react";
import { Link } from "react-router-dom";
import { Button } from "../Button";

function SectionFour() {
  return (
    <>
      <div className="home__hero-section darkBg">
        <div className="container">
          <div
            className="row home__hero-row"
            style={{
              display: "flex",
              flexDirection: "row-reverse",
            }}
          >
            <div className="col">
              <div className="home__hero-text-wrapper">
                <div className="top-line">Téléchargez l'appli</div>
                <h1 className="heading">Google play</h1>
                <p className="home__hero-subtitle">
                  L'application la plus simple, Cliquez sur un bouton pour pointer en entrée et commencer à suivre les
                  heures effectuées.
                </p>
                <Link to="//play.google.com/store/apps/details?id=com.pointeuse.chechin" target="_blank">
                  <Button buttonSize="btn--wide" buttonColor="blue">
                    Télecharger
                  </Button>
                </Link>
              </div>
            </div>
            <div className="col">
              <div className="home__hero-img-wrapper">
                <img src="assets/images/welcome/svg-2.svg" alt="google play" className="home__hero-img" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default SectionFour;
