import { useEffect } from "react";
import DashboardLayout from "./layout/DashboardLayout";
import AppRoute from "./Routes/AppRoute";
import { BrowserRouter, Link, Redirect, Switch } from "react-router-dom";
import EmptyLayout from "./layout/EmptyLayout";
import LoginLayout from "./layout/LoginLayout";
import PageNotFound from "./Pages/PageNotFound";
import SignIn from "./Pages/SignIn";
import PrivateRoute from "./Routes/PrivateRoute";
import PublicRoute from "./Routes/PublicRoute";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import Welcome from "./Components/welcome/Welcome";
import firebase from "./firebase";
import "./App.css";
import socket from "./Connection/ws";
import Search from "./Components/Search";
import { getSession } from "./Utils/SessionUtils";
import toast from "react-hot-toast";
import { getUserById } from "./Services/Pointeuse/UsersApi";
import ImageComponent from "./Components/Styles/ImageComponent";
import { Howl } from "howler";

import audio from "./sounds/msg.mp3";

import WizardConfig from "./Components/Config/WizardConfig";
import Main from "./Components/Main";

function App() {
  const history = useHistory();

  var msgSound = new Howl({
    src: ["./assets/sounds/msg.mp3"],
    loop: false,
    preload: true,
  });

  var notifSound = new Howl({
    src: ["./assets/sounds/notif.mp3"],
    loop: false,
    preload: true,
  });

  const playAudio = () => {
    new Audio(audio).play();
  };

  // let msgSound = new Audio("./assets/sounds/msg.mp3");
  // let notifSound = new Audio("./assets/sounds/notif.mp3");

  useEffect(() => {
    //msgSound.play();
    // audio1.play();
    // audio2.play();
  }, []);

  useEffect(() => {
    socket.on("pointage", (result) => {
      console.log(result);

      if (result.code_generated == getSession("code_generated")) {
        notifSound.play();
        getUserById(result.user_code_id).then((res) => {
          res.data.id &&
            toast.custom((t) => (
              <div className="" style={{ width: 500, borderRadius: "60px", backgroundColor: "#333" }}>
                <div className="p-3">
                  <div className="d-flex align-items-center mt-auto" style={{ float: "left" }}>
                    <ImageComponent atr={"avatar avatar-md brround mr-3"} picture={res.data.img}></ImageComponent>
                    <div>
                      <a href="#" className="font-weight-semibold">
                        {res.data.nom} {res.data.prenom}
                      </a>
                      <small className="d-block text-muted fa-12">Pointage : {result.date}</small>
                    </div>
                  </div>
                  <div className="border-l border-gray-200 mb-4" style={{ float: "right" }}>
                    <a href="#" onClick={() => toast.dismiss(t.id)} class="btn btn-pill btn-light">
                      voir
                    </a>
                  </div>
                </div>
              </div>
            ));
        });
      }
    });
  }, []);

  useEffect(() => {
    socket.on("conge", (result) => {
      console.log(result);

      if (result.code_generated == getSession("code_generated")) {
        notifSound.play();
        getUserById(result.user_code_id).then((res) => {
          res.data.id &&
            toast.custom((t) => (
              <div className="" style={{ width: 500, borderRadius: "60px", backgroundColor: "#333" }}>
                <div className="p-3">
                  <div className="d-flex align-items-center mt-auto" style={{ float: "left" }}>
                    <ImageComponent atr={"avatar avatar-md brround mr-3"} picture={res.data.img}></ImageComponent>
                    <div>
                      <a href="#" className="font-weight-semibold">
                        {res.data.nom} {res.data.prenom}
                      </a>
                      <small className="d-block text-muted fa-12">
                        Demande {result.motif} de {result.nbr_jour} J à partir de {result.date}
                      </small>
                    </div>
                  </div>
                  <div className="border-l border-gray-200 mb-4" style={{ float: "right" }}>
                    <a href="#" onClick={() => toast.dismiss(t.id)} class="btn btn-pill btn-light">
                      voir
                    </a>
                  </div>
                </div>
              </div>
            ));
        });
      }
    });
  }, []);

  useEffect(() => {
    socket.on("autorisation", (result) => {
      console.log(result);

      if (result.code_generated == getSession("code_generated")) {
        notifSound.play();
        getUserById(result.user_code_id).then((res) => {
          res.data.id &&
            toast.custom((t) => (
              <div className="" style={{ width: 500, borderRadius: "60px", backgroundColor: "#333" }}>
                <div className="p-3">
                  <div className="d-flex align-items-center mt-auto" style={{ float: "left" }}>
                    <ImageComponent atr={"avatar avatar-md brround mr-3"} picture={res.data.img}></ImageComponent>
                    <div>
                      <a href="#" className="font-weight-semibold">
                        {res.data.nom} {res.data.prenom}
                      </a>
                      <small className="d-block text-muted fa-12">
                        Demande autorisation {result.objet} de {result.nb_heure} H à partir de {result.date}{" "}
                        {result.heure_debut}
                      </small>
                    </div>
                  </div>
                  <div className="border-l border-gray-200 mb-4" style={{ float: "right" }}>
                    <a href="#" onClick={() => toast.dismiss(t.id)} class="btn btn-pill btn-light">
                      voir
                    </a>
                  </div>
                </div>
              </div>
            ));
        });
      }
    });
  }, []);

  useEffect(() => {
    socket.on("retard", (result) => {
      console.log(result);

      if (result.code_generated == getSession("code_generated")) {
        notifSound.play();
        getUserById(result.user_code_id).then((res) => {
          res.data.id &&
            toast.custom((t) => (
              <div className="" style={{ width: 500, borderRadius: "60px", backgroundColor: "#333" }}>
                <div className="p-3">
                  <div className="d-flex align-items-center mt-auto" style={{ float: "left" }}>
                    <ImageComponent atr={"avatar avatar-md brround mr-3"} picture={res.data.img}></ImageComponent>
                    <div>
                      <a href="#" className="font-weight-semibold">
                        {res.data.nom} {res.data.prenom}
                      </a>
                      <small className="d-block text-muted fa-12">Retard de {result.time}</small>
                    </div>
                  </div>
                  <div className="border-l border-gray-200 mb-4" style={{ float: "right" }}>
                    <a href="#" onClick={() => toast.dismiss(t.id)} class="btn btn-pill btn-light">
                      voir
                    </a>
                  </div>
                </div>
              </div>
            ));
        });
      }
    });
  }, []);

  useEffect(() => {
    socket.on("questionnaire", (result) => {
      console.log(result);

      if (result.code_generated == getSession("code_generated")) {
        notifSound.play();
        getUserById(result.user_code_id).then((res) => {
          res.data.id &&
            toast.custom((t) => (
              <div className="" style={{ width: 500, borderRadius: "60px", backgroundColor: "#333" }}>
                <div className="p-3">
                  <div className="d-flex align-items-center mt-auto" style={{ float: "left" }}>
                    <ImageComponent atr={"avatar avatar-md brround mr-3"} picture={res.data.img}></ImageComponent>
                    <div>
                      <a href="#" className="font-weight-semibold">
                        {res.data.nom} {res.data.prenom}
                      </a>
                      <small className="d-block text-muted fa-12">
                        Questionnaire pour un retard de {result.retardTime} : {result.motif}
                      </small>
                    </div>
                  </div>
                  <div className="border-l border-gray-200 mb-4" style={{ float: "right" }}>
                    <a href="#" onClick={() => toast.dismiss(t.id)} class="btn btn-pill btn-light">
                      voir
                    </a>
                  </div>
                </div>
              </div>
            ));
        });
      }
    });
  }, []);

  useEffect(() => {
    socket.on("chat", (result) => {
      console.log(result);
      if (result.code_generated == getSession("code_generated") && result.id_sender != getSession("id")) {
        //alert("sound");
        msgSound.play();
        //playAudio();
        getUserById(result.id_sender).then((res) => {
          res.data.id &&
            toast.custom((t) => (
              <div className="" style={{ width: 500, borderRadius: "60px", backgroundColor: "#333" }}>
                <div className="p-3">
                  <div className="d-flex align-items-center mt-auto" style={{ float: "left" }}>
                    <ImageComponent atr={"avatar avatar-md brround mr-3"} picture={res.data.img}></ImageComponent>
                    <div>
                      <a href="#" className="font-weight-semibold">
                        {res.data.nom} {res.data.prenom}
                      </a>
                      <small className="d-block text-muted fa-12">{result.msg}</small>
                    </div>
                  </div>
                  <div className="border-l border-gray-200 mb-4" style={{ float: "right" }}>
                    <a href="#" onClick={() => toast.dismiss(t.id)} class="btn btn-pill btn-light">
                      voir
                    </a>
                  </div>
                </div>
              </div>
            ));
        });
      }
    });
  }, []);

  useEffect(() => {
    if (localStorage.getItem("pointeuse_dark") === "true") {
      document.body.classList.add("dark-mode");
    } else {
      document.body.classList.remove("dark-mode");
    }
  }, []);

  useEffect(() => {
    try {
      const messaging = firebase.messaging();
      const topic = localStorage.getItem("code_generated") + "admin";

      messaging
        .requestPermission()
        .then(() => {
          return messaging.getToken();
        })
        .then((token) => {
          subscribeTokenToTopic(token, topic);
          subscribeTokenToTopic(token, localStorage.getItem("code_generated") + "user" + localStorage.getItem("id"));
        })
        .catch((err) => {
          console.log(err);
        });
      messaging.onMessage((payload) => {
        showNotification(payload);
      });
    } catch (error) {
      console.error(error);
      // expected output: ReferenceError: nonExistentFunction is not defined
      // Note - error messages will vary depending on browser
    }
  }, []);

  function subscribeTokenToTopic(token, topic) {
    const fcm_server_key =
      "AAAAyFwzY3I:APA91bGIPThuQusFacH13cip7zdnDNY81mhV1xJV5pQXjqoHaIUwcibidgFahbxnjAxWODcIR0pew1a3tYlVUQNPLpx7nIanisZieGr0ZaMcwSqDvmcvyr9YjJK3yfoEbwu82sWDhL7E";
    fetch("https://iid.googleapis.com/iid/v1/" + token + "/rel/topics/" + topic, {
      method: "POST",
      headers: new Headers({
        Authorization: "key=" + fcm_server_key,
      }),
    })
      .then((response) => {
        if (response.status < 200 || response.status >= 400) {
          throw "Error subscribing to topic: " + response.status + " - " + response.text();
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }

  const showNotification = (payload) => {
    var options = {
      body: payload.data.message,
      icon: "/dist/img/logo.png?    auto=compress&cs=tinysrgb&dpr=1&w=500",
      dir: "ltr",
    };
    new Notification(payload.data.title, options);
  };

  return (
    <BrowserRouter>
      <Switch>
        <AppRoute layout={EmptyLayout} component={Welcome} path="/welcome" exact />
        <PrivateRoute layout={DashboardLayout} component={Main} path="/" exact />
        <PublicRoute layout={LoginLayout} component={SignIn} path="/login" exact />
        <PrivateRoute path="/admin/search" layout={DashboardLayout} component={Search} />
        <PrivateRoute path="/admin/config" layout={EmptyLayout} component={WizardConfig}></PrivateRoute>
        <AppRoute layout={EmptyLayout} component={PageNotFound} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
