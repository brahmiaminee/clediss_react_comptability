/**
 * check if string is upercase
 * @param {string} str
 * @returns
 */
export function hasUperCase(str) {
  return str.toLowerCase() != str;
}

/**
 * check if string has a number
 * @param {String} myString
 * @returns
 */
export function hasNumber(myString) {
  return /\d/.test(myString);
}

/**
 * check if string have a symbol
 * @param {String} myString
 * @returns
 */
export function hasSymbole(myString) {
  var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  if (format.test(myString)) {
    return true;
  } else {
    return false;
  }
}
