/**
 * return token
 */
export function getToken() {
  return localStorage.getItem("token");
}
/**
 *
 * @param {String} value
 * @returns booleen
 */
export function getSession(value) {
  return localStorage.getItem(value);
}

/**
 * check if user is connected
 * @returns booleen
 */
export const isLogin = () => {
  if (getSession("isLoginPointeuse") == "true") {
    return true;
  }
  return false;
};

/**
 * check if admin is connected
 * @returns booleen
 */
export const isAdmin = () => {
  if (getSession("role_code") == "admin") {
    return true;
  }
  return false;
};
