import { calculateCroissance } from "../ObjectifUtils";

export function getObjectData(result) {
  return [
    {
      mois: "janvier",
      realisation1: result[12]?.realise,
      realisation2: result[0]?.realise,
      objectif: result[24]?.objectif ?? "0",
      croissance: calculateCroissance(result[24]?.objectif ?? "0", result[12]?.realise),
    },
    {
      mois: "février",
      realisation1: result[13]?.realise,
      realisation2: result[1]?.realise,
      objectif: result[25]?.objectif ?? "0",
      croissance: calculateCroissance(result[25]?.objectif ?? "0", result[13]?.realise),
    },
    {
      mois: "mars",
      realisation1: result[14]?.realise,
      realisation2: result[2]?.realise,
      objectif: result[26]?.objectif ?? "0",
      croissance: calculateCroissance(result[26]?.objectif ?? "0", result[14]?.realise),
    },
    {
      mois: "avril",
      realisation1: result[15]?.realise,
      realisation2: result[3]?.realise,
      objectif: result[27]?.objectif ?? "0",
      croissance: calculateCroissance(result[27]?.objectif ?? "0", result[15]?.realise),
    },
    {
      mois: "mai",
      realisation1: result[16]?.realise,
      realisation2: result[4]?.realise,
      objectif: result[28]?.objectif ?? "0",
      croissance: calculateCroissance(result[28]?.objectif ?? "0", result[16]?.realise),
    },
    {
      mois: "juin",
      realisation1: result[17]?.realise,
      realisation2: result[5]?.realise,
      objectif: result[29]?.objectif ?? "0",
      croissance: calculateCroissance(result[29]?.objectif ?? "0", result[17]?.realise),
    },
    {
      mois: "juillet",
      realisation1: result[18]?.realise,
      realisation2: result[6]?.realise,
      objectif: result[30]?.objectif ?? "0",
      croissance: calculateCroissance(result[30]?.objectif ?? "0", result[18]?.realise),
    },
    {
      mois: "aout",
      realisation1: result[19]?.realise,
      realisation2: result[7]?.realise,
      objectif: result[31]?.objectif ?? "0",
      croissance: calculateCroissance(result[31]?.objectif ?? "0", result[19]?.realise),
    },
    {
      mois: "septembre",
      realisation1: result[20]?.realise,
      realisation2: result[8]?.realise,
      objectif: result[32]?.objectif ?? "0",
      croissance: calculateCroissance(result[32]?.objectif ?? "0", result[20]?.realise),
    },
    {
      mois: "octobre",
      realisation1: result[21]?.realise,
      realisation2: result[9]?.realise,
      objectif: result[33]?.objectif ?? "0",
      croissance: calculateCroissance(result[33]?.objectif ?? "0", result[21]?.realise),
    },
    {
      mois: "novembre",
      realisation1: result[22]?.realise,
      realisation2: result[10]?.realise,
      objectif: result[34]?.objectif ?? "0",
      croissance: calculateCroissance(result[34]?.objectif ?? "0", result[22]?.realise),
    },
    {
      mois: "décembre",
      realisation1: result[23]?.realise,
      realisation2: result[11]?.realise,
      objectif: result[35]?.objectif ?? "0",
      croissance: calculateCroissance(result[35]?.objectif ?? "0", result[23]?.realise),
    },
  ];
}
