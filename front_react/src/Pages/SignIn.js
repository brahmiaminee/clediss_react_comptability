import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { userLogin } from "../Services/Pointeuse/UsersApi";
import { useAlert } from "react-alert";

function SignIn() {
  const alert = useAlert();
  const history = useHistory();
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");
  const [loading, setloading] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    userLogin(email, password).then((response) => {
      if (response.data.success == "0") {
        alert.error(response.data.data);
      } else {
        if (response.data.results.role_code === "admin") {
          if (response.data.results.code_generated) {
            setloading(true);
            localStorage.setItem("token", response.data.token);
            localStorage.setItem("nom", response.data.results.nom);
            localStorage.setItem("prenom", response.data.results.prenom);
            localStorage.setItem("id", response.data.results.id);
            localStorage.setItem("img", response.data.results.img);
            localStorage.setItem("code_generated", response.data.results.code_generated);
            localStorage.setItem("email", response.data.results.email);
            localStorage.setItem("role_code", response.data.results.role_code);
            localStorage.setItem("isLoginPointeuse", true);
            localStorage.setItem("baseUrl", process.env.REACT_APP_API);
            setTimeout(function () {
              history.push("/");
            }, 4000);
          } else {
            setloading(true);
            localStorage.setItem("isLoginPointeuse", true);
            localStorage.setItem("token", response.data.token);
            localStorage.setItem("id", response.data.results.id);
            setTimeout(function () {
              history.push("/admin/config");
            }, 4000);
          }
        } else {
          // localStorage.setItem("token", response.data.token);
          // localStorage.setItem("nom", response.data.results.nom);
          // localStorage.setItem("prenom", response.data.results.prenom);
          // localStorage.setItem("id", response.data.results.id);
          // localStorage.setItem("img", response.data.results.img);
          // localStorage.setItem("code_generated", response.data.results.code_generated);
          // localStorage.setItem("email", response.data.results.email);
          // localStorage.setItem("role_code", response.data.results.role_code);
          // localStorage.setItem("isLoginPointeuse", true);
          // localStorage.setItem("baseUrl", process.env.REACT_APP_API);
          //history.push("/user");
          alert.error("compte utilisateur");
        }
      }
    });
  };
  return (
    <div className="page relative error-page3">
      <div className="row no-gutters">
        <div className="col-xl-6 h-100vh">
          <div className="cover-image h-100vh">
            <div className="container">
              <div className="customlogin-imgcontent">
                <h2 className="mb-3 fs-35 text-white">Bienvenue à la Pointeuse</h2>
                <p className="text-white-50">
                  Choisissez la solution de gestion RH la plus pratique, la plus facile à utiliser et la plus simple à
                  paramétrer.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="col-xl-6 bg-white h-100vh">
          <div className="container">
            <div className="customlogin-content">
              {/* <div className="pt-4 pb-2">
                <a className="header-brand" href="index.html">
                  <img
                    src="assets/images/brand/logo.png"
                    style={{ widh: "50px", height: "50px" }}
                    className="header-brand-img custom-logo"
                    alt="Dayonelogo"
                  />
                  <img
                    src="assets/images/brand/logo-white.png"
                    className="header-brand-img custom-logo-dark"
                    alt="Dayonelogo"
                  />
                </a>
              </div> */}
              <div className="p-4 pt-6">
                <h1 className="mb-2">Se connecter</h1>
                <p className="text-muted">Connectez-vous à votre compte</p>
              </div>
              <form className="card-body pt-3" id="login" name="login">
                <div className="form-group">
                  <label className="form-label">Email</label>
                  <input
                    className="form-control"
                    placeholder="Email"
                    type="email"
                    onChange={(e) => setemail(e.target.value)}
                  />
                </div>
                <div className="form-group">
                  <label className="form-label">Mot de passe</label>
                  <input
                    className="form-control"
                    placeholder="mot de passe"
                    type="password"
                    onChange={(e) => setpassword(e.target.value)}
                  />
                </div>
                {/* <div className="form-group">
                  <label className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      name="example-checkbox1"
                      defaultValue="option1"
                    />
                    <span className="custom-control-label">Remeber me</span>
                  </label>
                </div> */}
                <div className="submit">
                  {loading ? (
                    <button className="btn btn-info btn-loading btn-block">Button text</button>
                  ) : (
                    <Link to="#" className="btn btn-primary btn-block" onClick={handleSubmit}>
                      Connecter
                    </Link>
                  )}
                </div>
                <div className="text-center mt-3">
                  {/* <p className="mb-2">
                    <a href="#">Forgot Password</a>
                  </p> */}
                  <p className="text-dark mb-0">
                    Je n'ai pas de compte?
                    <Link
                      to="#"
                      data-toggle="tooltip"
                      data-placement="top"
                      title="contact@nomadis.online"
                      className="text-primary ml-1"
                      onClick={(e) => {
                        window.location = "mailto:contact@nomadis.online?subject = Feedback&body = Message";
                        e.preventDefault();
                      }}
                    >
                      Demande d'accès
                    </Link>
                  </p>
                </div>
              </form>
              {/* <div className="card-body border-top-0 pb-6 pt-2">
                <div className="text-center">
                  <span className="avatar brround mr-3 bg-primary-transparent text-primary">
                    <i className="ri-facebook-line" />
                  </span>
                  <span className="avatar brround mr-3 bg-primary-transparent text-primary">
                    <i className="ri-instagram-line" />
                  </span>
                  <span className="avatar brround mr-3 bg-primary-transparent text-primary">
                    <i className="ri-twitter-line" />
                  </span>
                </div>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SignIn;
